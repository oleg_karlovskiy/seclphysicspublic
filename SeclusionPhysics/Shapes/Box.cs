using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;
using Mathf = SeclusionPhysics.Utils.Mathf;

namespace SeclusionPhysics.Shapes
{
    public class Box : SeclShape
    {
        public override ShapeType Type => ShapeType.Box;
        
        private Transform _localTransform;
        private Vector3 _e; // extent, as in the extent of each OBB axis
        
        private Transform _worldTransform;
        
        static Vector3[] kBoxVertices = {
            new Vector3( -1, -1, -1 ),
            new  Vector3( -1, -1,  1 ),
            new  Vector3( -1,  1, -1 ),
            new  Vector3( -1,  1,  1 ),
            new  Vector3(  1, -1, -1 ),
            new  Vector3(  1, -1,  1 ),
            new  Vector3(  1,  1, -1 ),
            new  Vector3(  1,  1,  1 )
        };

        protected override void ComputeMetrics()
        {
            // If we were initialized with world points, we need to compute body
            if (_boundsSet)
            {
                bodySpaceAABB = ComputeBounds(_localTransform, _e);
                CalculateMassInertia();
            }
        }
        
        private void CalculateMassInertia()
        {
            var size = _e * 2;
            mass = size.x * size.y * size.z;

            inertia = Mat3.Identity;
            inertia.ex.x = (1.0f / 12.0f) * mass * (size.y * size.y + size.z * size.z);
            inertia.ey.y = (1.0f / 12.0f) * mass * (size.x * size.x + size.z * size.z);
            inertia.ez.z = (1.0f / 12.0f) * mass * (size.x * size.x + size.y * size.y);

            geomCen = Vector3.zero;
        }

        protected override void ApplyBodyPosition()
        {
            _worldTransform = Transform.Mul(Body.Transform, _localTransform);
            worldSpaceAABB = ComputeBounds(_worldTransform, _e);
        }

        private static VoltAABB ComputeBounds(Transform t, Vector3 e)
        {            
            var min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            var max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            for (int i = 0; i < 8; ++i)
            {
                var v = Transform.Mul(t, Vector3.Mul(kBoxVertices[i], e));
                min = Vector3.Min(min, v);
                max = Vector3.Max(max, v);
            }

            return new VoltAABB {Min = min, Max = max};
        }

        protected override bool ShapeQueryPoint(Vector3 bodySpacePoint)
        {
            return false;
        }

        public override void DebugDraw(IDebugDrawer d, Color c)
        {
            d.SetColor(Body.CollidedBodies.Count == 0 ? c : Color.green);
            d.DrawBox(_worldTransform, _e);
        }

        public override void SupportMapping(ref Vector3 direction, out Vector3 result)
        {
            result.x = Mathf.Sign(direction.x) * _e.x;
            result.y = Mathf.Sign(direction.y) * _e.y;
            result.z = Mathf.Sign(direction.z) * _e.z;
        }

        private bool _boundsSet;
        public void InitializeFromBodySpace(Transform t, Vector3 e)
        {
            _localTransform = t;
            _e = e;
            _boundsSet = true;
        }

        protected override void Reset()
        {
            base.Reset();
            _boundsSet = false;
        }
    }
}