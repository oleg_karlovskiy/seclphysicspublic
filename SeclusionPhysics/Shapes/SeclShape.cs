﻿using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Shapes
{
    public abstract class SeclShape : IVoltPoolable<SeclShape>,  ISupportMappable
    {
        #region Interface

        ISeclPool<SeclShape> IVoltPoolable<SeclShape>.Pool { get; set; }

        void IVoltPoolable<SeclShape>.Reset()
        {
            Reset();
        }

        #endregion

        #region Static Methods

        internal static void OrderShapes(ref SeclShape sa, ref SeclShape sb)
        {
            if (sa.Type > sb.Type)
            {
                SeclShape temp = sa;
                sa = sb;
                sb = temp;
            }
        }

        #endregion

        public enum ShapeType
        {
            Ball,
            Polyhedron,
            Box,
            Terrain
        }

        public abstract ShapeType Type { get; }

        public object UserData { get; set; }
        public SeclBody Body { get; private set; }

        public VoltAABB AABB => worldSpaceAABB;

        // Body-space bounding AABB for pre-checks during queries/casts
        internal VoltAABB worldSpaceAABB;
        internal VoltAABB bodySpaceAABB;
        
        internal Mat3 inertia = Mat3.Identity;
        internal float mass = 1.0f;
        internal Vector3 geomCen = Vector3.zero;

        #region Body-Related

        internal void AssignBody(SeclBody body)
        {
            Body = body;
            ComputeMetrics();
        }

        internal void OnBodyPositionUpdated()
        {
            ApplyBodyPosition();
        }
        
        public virtual void GetBoundingBox(ref Mat3 orientation, out VoltAABB box)
        {
            // I don't think that this can be done faster.
            // 6 is the minimum number of SupportMap calls.

            Vector3 vec = Vector3.zero;
            
            box = new VoltAABB();
            
            vec.Set(orientation.ex.x, orientation.ey.x, orientation.ez.x);
            SupportMapping(ref vec, out vec);
            box.Max.x = orientation.ex.x * vec.x + orientation.ey.x * vec.y + orientation.ez.x * vec.z;

            vec.Set(orientation.ex.y, orientation.ey.y, orientation.ez.y);
            SupportMapping(ref vec, out vec);
            box.Max.y = orientation.ex.y * vec.x + orientation.ey.y * vec.y + orientation.ez.y * vec.z;

            vec.Set(orientation.ex.z, orientation.ey.z, orientation.ez.z);
            SupportMapping(ref vec, out vec);
            box.Max.z = orientation.ex.z * vec.x + orientation.ey.z * vec.y + orientation.ez.z * vec.z;

            vec.Set(-orientation.ex.x, -orientation.ey.x, -orientation.ez.x);
            SupportMapping(ref vec, out vec);
            box.Min.x = orientation.ex.x * vec.x + orientation.ey.x * vec.y + orientation.ez.x * vec.z;

            vec.Set(-orientation.ex.y, -orientation.ey.y, -orientation.ez.y);
            SupportMapping(ref vec, out vec);
            box.Min.y = orientation.ex.y * vec.x + orientation.ey.y * vec.y + orientation.ez.y * vec.z;

            vec.Set(-orientation.ex.z, -orientation.ey.z, -orientation.ez.z);
            SupportMapping(ref vec, out vec);
            box.Min.z = orientation.ex.z * vec.x + orientation.ey.z * vec.y + orientation.ez.z * vec.z;
        }

        #endregion

        #region Tests

        internal bool QueryPoint(Vector3 bodySpacePoint)
        {
            // Queries and casts on shapes are always done in body space
            if (bodySpaceAABB.QueryPoint(bodySpacePoint))
                return ShapeQueryPoint(bodySpacePoint);
            return false;
        }

        #endregion

        protected virtual void Reset()
        {
            UserData = null;
            Body = null;

            bodySpaceAABB = default(VoltAABB);
            worldSpaceAABB = default(VoltAABB);
        }

        #region Functionality Overrides

        protected abstract void ComputeMetrics();
        protected abstract void ApplyBodyPosition();

        protected abstract bool ShapeQueryPoint(Vector3 bodySpacePoint);

        #endregion

        public abstract void DebugDraw(IDebugDrawer d, Color c);

        public abstract void SupportMapping(ref Vector3 direction, out Vector3 result);

        private Vector3 _centerCache;
        private bool _centerCached;
        
        public void SupportCenter(out Vector3 center)
        {
            center = geomCen;
        }
    }
}