using System.Collections.Generic;
using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Shapes
{
    public abstract class Multishape : SeclShape
    {

        /// <summary>
        /// Sets the current shape. First <see cref="Prepare"/> has to be called.
        /// After SetCurrentShape the shape immitates another shape.
        /// </summary>
        /// <param name="index"></param>
        public abstract void SetCurrentShape(int index);

        /// <summary>
        /// Passes a axis aligned bounding box to the shape where collision
        /// could occour.
        /// </summary>
        /// <param name="box">The bounding box where collision could occur.</param>
        /// <returns>The upper index with which <see cref="SetCurrentShape"/> can be 
        /// called.</returns>
        public abstract int Prepare(ref VoltAABB box);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rayOrigin"></param>
        /// <param name="rayDelta"></param>
        /// <returns></returns>
        public abstract int Prepare(ref Vector3 rayOrigin, ref Vector3 rayDelta);

        protected abstract Multishape CreateWorkingClone();

        internal bool isClone = false;

        public bool IsClone { get{ return isClone;} }

        Stack<Multishape> workingCloneStack = new Stack<Multishape>(); //todo: create new if this will be not clone
        public Multishape RequestWorkingClone()
        {
            SeclDebug.Assert(workingCloneStack.Count<10, "Unusual size of the workingCloneStack. Forgot to call ReturnWorkingClone?");
            SeclDebug.Assert(!isClone, "Can't clone clones! Something wrong here!");

            Multishape multiShape;

            if (workingCloneStack.Count == 0)
            {
                multiShape = CreateWorkingClone();
                multiShape.workingCloneStack = workingCloneStack;
                workingCloneStack.Push(multiShape);
            }
            multiShape = workingCloneStack.Pop();
            multiShape.isClone = true;

            return multiShape;
        }

        protected override void ComputeMetrics()
        {
            workingCloneStack.Clear();
        }

        public void ReturnWorkingClone()
        {
            SeclDebug.Assert(isClone, "Only clones can be returned!");
            workingCloneStack.Push(this);
        }

        /// <summary>
        /// Gets the axis aligned bounding box of the orientated shape. This includes
        /// the whole shape.
        /// </summary>
        /// <param name="orientation">The orientation of the shape.</param>
        /// <param name="box">The axis aligned bounding box of the shape.</param>
        public override void GetBoundingBox(ref Mat3 orientation, out VoltAABB box)
        {
            VoltAABB helpBox = VoltAABB.LargeBox;
            int length = Prepare(ref helpBox);

            box = VoltAABB.SmallBox;

            for (int i = 0; i < length; i++)
            {
                SetCurrentShape(i);
                base.GetBoundingBox(ref orientation, out helpBox);
                box = VoltAABB.CreateMerged(helpBox, box);
            }
        }

        /// <summary>
        /// Calculates the inertia of a box with the sides of the multishape.
        /// </summary>
        public void CalculateMassInertia()
        {
            geomCen = Vector3.zero;

            // TODO: calc this right
            inertia = Mat3.Identity;

            Vector3 size; Vector3.Subtract(ref bodySpaceAABB.Max, ref bodySpaceAABB.Min, out size);

            mass = size.x * size.y * size.z;

            inertia.ex.x = (1.0f / 12.0f) * mass * (size.y * size.y + size.z * size.z);
            inertia.ey.y = (1.0f / 12.0f) * mass * (size.x * size.x + size.z * size.z);
            inertia.ez.z = (1.0f / 12.0f) * mass * (size.x * size.x + size.y * size.y);
        }
    }
}