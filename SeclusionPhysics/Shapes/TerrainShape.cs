using System;
using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Shapes
{
    public class TerrainShape : Multishape
    {
        private float[,] heights;
        private float scaleX, scaleZ;
        private int heightsLength0, heightsLength1;

        private int minX, maxX;
        private int minZ, maxZ;
        private int numX, numZ;

        /// <summary>
        /// Expands the triangles by the specified amount.
        /// This stabilizes collision detection for flat shapes.
        /// </summary>
        private float sphericalExpansion = 0.5f;

        public void Initialize(float[,] heights, float scaleX, float scaleZ)
        {
            this.sphericalExpansion = 0.05f;
            
            heightsLength0 = heights.GetLength(0);
            heightsLength1 = heights.GetLength(1);

            #region Bounding Box
            bodySpaceAABB = VoltAABB.SmallBox;

            for (int i = 0; i < heightsLength0; i++)
            {
                for (int e = 0; e < heightsLength1; e++)
                {
                    if (heights[i, e] > bodySpaceAABB.Max.y)
                        bodySpaceAABB.Max.y = heights[i, e];
                    else if (heights[i, e] < bodySpaceAABB.Min.y)
                        bodySpaceAABB.Min.y = heights[i, e];
                }
            }

            bodySpaceAABB.Min.x = 0;
            bodySpaceAABB.Min.z = 0;

            bodySpaceAABB.Max.x = heightsLength0 * scaleX;
            bodySpaceAABB.Max.z = heightsLength1 * scaleZ;

            #endregion

            this.heights = heights;
            this.scaleX = scaleX;
            this.scaleZ = scaleZ;
            
            ComputeMetrics();
        }

        protected sealed override void ComputeMetrics()
        {
            base.ComputeMetrics();
            var qwe = Mat3.Identity;
            GetBoundingBox(ref qwe, out bodySpaceAABB);

            CalculateMassInertia();            
        }

        public TerrainShape() { }


        protected override Multishape CreateWorkingClone()
        {
            TerrainShape clone = new TerrainShape();
            clone.heights = this.heights;
            clone.bodySpaceAABB = this.bodySpaceAABB;
            clone.scaleX = this.scaleX;
            clone.scaleZ = this.scaleZ;
            clone.worldSpaceAABB = this.worldSpaceAABB;
            clone.heightsLength0 = this.heightsLength0;
            clone.heightsLength1 = this.heightsLength1;
            clone.sphericalExpansion = this.sphericalExpansion;
            clone.AssignBody(this.Body);
            return clone;
        }


        private Vector3[] points = new Vector3[3];
        private Vector3 normal = Vector3.up;

        /// <summary>
        /// Sets the current shape. First <see cref="Prepare"/> has to be called.
        /// After SetCurrentShape the shape immitates another shape.
        /// </summary>
        /// <param name="index"></param>
        public override void SetCurrentShape(int index)
        {
            bool leftTriangle = false;

            if (index >= numX * numZ)
            {
                leftTriangle = true;
                index -= numX * numZ;
            }

            int quadIndexX = index % numX;
            int quadIndexZ = index / numX;

            // each quad has two triangles, called 'leftTriangle' and !'leftTriangle'
            if (leftTriangle)
            {
                points[0].Set((minX + quadIndexX + 0) * scaleX, heights[minX + quadIndexX + 0, minZ + quadIndexZ + 0], (minZ + quadIndexZ + 0) * scaleZ);
                points[1].Set((minX + quadIndexX + 1) * scaleX, heights[minX + quadIndexX + 1, minZ + quadIndexZ + 0], (minZ + quadIndexZ + 0) * scaleZ);
                points[2].Set((minX + quadIndexX + 0) * scaleX, heights[minX + quadIndexX + 0, minZ + quadIndexZ + 1], (minZ + quadIndexZ + 1) * scaleZ);
            }
            else
            {
                points[0].Set((minX + quadIndexX + 1) * scaleX, heights[minX + quadIndexX + 1, minZ + quadIndexZ + 0], (minZ + quadIndexZ + 0) * scaleZ);
                points[1].Set((minX + quadIndexX + 1) * scaleX, heights[minX + quadIndexX + 1, minZ + quadIndexZ + 1], (minZ + quadIndexZ + 1) * scaleZ);
                points[2].Set((minX + quadIndexX + 0) * scaleX, heights[minX + quadIndexX + 0, minZ + quadIndexZ + 1], (minZ + quadIndexZ + 1) * scaleZ);
            }

            Vector3 sum = points[0];
            Vector3.Add(ref sum, ref points[1], out sum);
            Vector3.Add(ref sum, ref points[2], out sum);
            Vector3.Multiply(ref sum, 1.0f / 3.0f, out sum);
            geomCen = sum;

            Vector3.Subtract(ref points[1], ref points[0], out sum);
            Vector3.Subtract(ref points[2], ref points[0], out normal);
            Vector3.Cross(ref sum, ref normal, out normal);
        }

        public void CollisionNormal(out Vector3 normal)
        {
            normal = this.normal;
        }


        /// <summary>
        /// Passes a axis aligned bounding box to the shape where collision
        /// could occour.
        /// </summary>
        /// <param name="box">The bounding box where collision could occur.</param>
        /// <returns>The upper index with which <see cref="SetCurrentShape"/> can be 
        /// called.</returns>
        public override int Prepare(ref VoltAABB box)
        {
            // simple idea: the terrain is a grid. x and z is the position in the grid.
            // y the height. we know compute the min and max grid-points. All quads
            // between these points have to be checked.

            // including overflow exception prevention

            if (box.Min.x < bodySpaceAABB.Min.x) minX = 0;
            else
            {
                minX = (int)Math.Floor((float)((box.Min.x - sphericalExpansion) / scaleX));
                minX = Math.Max(minX, 0);
            }

            if (box.Max.x > bodySpaceAABB.Max.x) maxX = heightsLength0 - 1;
            else
            {
                maxX = (int)Math.Ceiling((float)((box.Max.x + sphericalExpansion) / scaleX));
                maxX = Math.Min(maxX, heightsLength0 - 1);
            }

            if (box.Min.z < bodySpaceAABB.Min.z) minZ = 0;
            else
            {
                minZ = (int)Math.Floor((float)((box.Min.z - sphericalExpansion) / scaleZ));
                minZ = Math.Max(minZ, 0);
            }

            if (box.Max.z > bodySpaceAABB.Max.z) maxZ = heightsLength1 - 1;
            else
            {
                maxZ = (int)Math.Ceiling((float)((box.Max.z + sphericalExpansion) / scaleZ));
                maxZ = Math.Min(maxZ, heightsLength1 - 1);
            }

            numX = maxX - minX;
            numZ = maxZ - minZ;

            // since every quad contains two triangles we multiply by 2.
            return numX * numZ * 2;
        }

        public void CalculateMassInertia()
        {
            this.inertia = Mat3.Identity;
            this.mass = 1.0f;
        }

        /// <summary>
        /// Gets the axis aligned bounding box of the orientated shape. This includes
        /// the whole shape.
        /// </summary>
        /// <param name="orientation">The orientation of the shape.</param>
        /// <param name="box">The axis aligned bounding box of the shape.</param>
        public override void GetBoundingBox(ref Mat3 orientation, out VoltAABB box)
        {
            box = bodySpaceAABB;

            #region Expand Spherical
            box.Min.x -= sphericalExpansion;
            box.Min.y -= sphericalExpansion;
            box.Min.z -= sphericalExpansion;
            box.Max.x += sphericalExpansion;
            box.Max.y += sphericalExpansion;
            box.Max.z += sphericalExpansion;
            #endregion

            box.Transform(ref orientation);
        }

        /// <summary>
        /// SupportMapping. Finds the point in the shape furthest away from the given direction.
        /// Imagine a plane with a normal in the search direction. Now move the plane along the normal
        /// until the plane does not intersect the shape. The last intersection point is the result.
        /// </summary>
        /// <param name="direction">The direction.</param>
        /// <param name="result">The result.</param>
        public override void SupportMapping(ref Vector3 direction, out Vector3 result)
        {
            Vector3 expandVector;
            Vector3.Normalize(ref direction, out expandVector);
            Vector3.Multiply(ref expandVector, sphericalExpansion, out expandVector);

            int minIndex = 0;
            float min = Vector3.Dot(ref points[0], ref direction);
            float dot = Vector3.Dot(ref points[1], ref direction);
            if (dot > min)
            {
                min = dot;
                minIndex = 1;
            }
            dot = Vector3.Dot(ref points[2], ref direction);
            if (dot > min)
            {
                min = dot;
                minIndex = 2;
            }

            Vector3.Add(ref points[minIndex], ref expandVector, out result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rayOrigin"></param>
        /// <param name="rayDelta"></param>
        /// <returns></returns>
        public override int Prepare(ref Vector3 rayOrigin, ref Vector3 rayDelta)
        {
            VoltAABB box = VoltAABB.SmallBox;

            #region RayEnd + Expand Spherical
            Vector3 rayEnd;
            Vector3.Normalize(ref rayDelta, out rayEnd);
            rayEnd = rayOrigin + rayDelta + rayEnd * sphericalExpansion;
            #endregion

            box.AddPoint(ref rayOrigin);
            box.AddPoint(ref rayEnd);

            return this.Prepare(ref box);
        }

        public override ShapeType Type => ShapeType.Box;
        protected override void ApplyBodyPosition()
        {
            worldSpaceAABB = bodySpaceAABB;
            worldSpaceAABB.Transform(ref Body.currentState.transform.position);
        }

        protected override bool ShapeQueryPoint(Vector3 bodySpacePoint)
        {
            return false;
        }

        public override void DebugDraw(IDebugDrawer d, Color c)
        {
            d.SetColor(Body.CollidedBodies.Count == 0 ? c : Color.green);
            var length0 = heights.GetLength(0);
            var length1 = heights.GetLength(1);

//            var halfWidth = (length0 / 2) * sphericalExpansion;
//            var halfHeight = (length1 / 2) * sphericalExpansion;
            
            for (var i = 0; i < length0; i++)
            {
                for (var j = 0; j < length1; j++)
                {
                     d.DrawSphere(new Vector3(
                         Body.currentState.transform.position.x + i * scaleX, 
                         Body.currentState.transform.position.y + heights[i, j],
                         Body.currentState.transform.position.z + j * scaleZ), 0.1f);
                }
            }
        }
    }
}