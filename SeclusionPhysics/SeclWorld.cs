using System.Collections.Generic;
using SeclusionPhysics.Internals.Broadphase;
using SeclusionPhysics.Internals.Collision;
using SeclusionPhysics.Internals.History;
using SeclusionPhysics.Shapes;
using SeclusionPhysics.Utils;
using Utils;
using Collision = SeclusionPhysics.Internals.Collision.Collision;
using Color = VionisEssencesSystem.GameCore.MiscStructs.Color;
using Transform = VionisEssencesSystem.GameCore.MiscStructs.Transform;
using Vector3 = VionisEssencesSystem.GameCore.MiscStructs.Vector3;

namespace SeclusionPhysics
{
    public class SeclWorld
    {
        private long ID_UK;
        
        public int HistoryLength { get; }
        internal float Damping { get; }
        
        public Vector3 Gravity = new Vector3(0, -9.81f, 0);

        private IBroadPhase dynamicBroadphase;
        private IBroadPhase staticBroadphase;
        
        private ISeclPool<SeclBody> dynamicBodies;
        private ISeclPool<SeclBody> staticBodies;
        
        private readonly Dictionary<SeclShape.ShapeType, ISeclPool<SeclShape>> shapesPools = new Dictionary<SeclShape.ShapeType, ISeclPool<SeclShape>>();
        
        private SeclBuffer<SeclBody> reusableBuffer;
        private SeclBuffer<SeclBody> reusableOutput;
        
        private ISeclPool<Manifold> manifoldPool;
        private ISeclPool<HistoryBuffer> historyPool;
        
        private List<Manifold> manifolds;

        public CheapList<SeclBody> Bodies;

        public SeclWorld(int historyLength = 0, float damping = SeclConfig.DEFAULT_LINEAR_DAMPING)
        {
            HistoryLength = historyLength;
            Damping = damping;
            
            dynamicBroadphase = new NaiveBroadphase();
            staticBroadphase = new TreeBroadphase();
            
            Bodies = new CheapList<SeclBody>();
            manifolds = new List<Manifold>();
            
            reusableBuffer = new SeclBuffer<SeclBody>();
            reusableOutput = new SeclBuffer<SeclBody>();
            
            staticBodies = new SeclPool<SeclBody>();
            dynamicBodies = new SeclPool<SeclBody>();
            
            shapesPools[SeclShape.ShapeType.Box] = new SeclPool<SeclShape, Box>();
            shapesPools[SeclShape.ShapeType.Terrain] = new SeclPool<SeclShape, TerrainShape>();
            
            manifoldPool = new SeclPool<Manifold>();
            historyPool = new SeclPool<HistoryBuffer>();
        }
        
        public SeclBody CreateDynamicBody(Transform t, params SeclShape[] shapesToAdd)
        {
            var body = dynamicBodies.Allocate();
            body.InitializeDynamic(t, shapesToAdd);
            AddBodyInternal(body);
            return body;
        }

        public SeclBody CreateStaticBody(Transform t, params SeclShape[] shapesToAdd)
        {
            var body = staticBodies.Allocate();
            body.InitializeStatic(t, shapesToAdd);
            AddBodyInternal(body);
            return body;
        }
        
        public void DestroyBody(SeclBody body)
        {
            SeclDebug.Assert(body.World == this);

            body.FreeShapes();

            RemoveBodyInternal(body);
            FreeBody(body);
        }
        
        private void AddBodyInternal(SeclBody body)
        {
            Bodies.Add(body);
            if (body.IsStatic)
                staticBroadphase.AddBody(body);
            else
                dynamicBroadphase.AddBody(body);

            body.Id = ID_UK++;
            body.AssignWorld(this);
            if (HistoryLength > 0 && !body.IsStatic)
                body.AssignHistory(AllocateHistory());
        }
        
        private void RemoveBodyInternal(SeclBody body)
        {
            Bodies.Remove(body);
            if (body.IsStatic)
                staticBroadphase.RemoveBody(body);
            else
                dynamicBroadphase.RemoveBody(body);

            body.FreeHistory();
            body.AssignWorld(null);
        }
        
        private void FreeBody(SeclBody body)
        {
            if (body.IsStatic)
                staticBodies.Deallocate(body);
            else
                dynamicBodies.Deallocate(body);
        }
        
        public Box CreateBoxBodySpace(Transform t, Vector3 e)
        {
            var box = (Box) shapesPools[SeclShape.ShapeType.Box].Allocate();
            box.InitializeFromBodySpace(t, e);
            return box;
        }
        
        public TerrainShape CreateTerrainShape(float[,] heights, float scaleX, float scaleZ)
        {
            var qwe = (TerrainShape) shapesPools[SeclShape.ShapeType.Terrain].Allocate();
            qwe.Initialize(heights, scaleX, scaleZ);
            return qwe;
        }
        
        private HistoryBuffer AllocateHistory()
        {
            var history = historyPool.Allocate();
            history.Initialize(HistoryLength);
            return history;
        }
        
        internal void FreeHistory(HistoryBuffer history)
        {
            historyPool.Deallocate(history);
        }

        public void Update()
        {
            for (int i = 0; i < Bodies.Count; i++)
            {
                var body = Bodies[i];
                if (!body.IsStatic && !body.isSleeping && !body.IsSharedBody)
                    body.onlyWithStaticImpact = true;
            }
            
            for (int i = 0; i < Bodies.Count; i++)
            {
                var body = Bodies[i];
                if (!body.IsSharedBody)
                {
                    body.CollidedBodies.Clear();
                    if (!body.IsStatic)
                    {
                        body.Update();
                        dynamicBroadphase.UpdateBody(body);
                    }
                }
            }
            
            for (int i = 0; i < Bodies.Count; i++)
            {
                var body = Bodies[i];
                if (!body.IsStatic && body.onlyWithStaticImpact && !body.isSleeping && !body.IsSharedBody)
                {
                    body.onlyWithStaticImpactCount++;
                    if (body.History.TryGet(1, out var val))
                    {
                        body.withStaticImpactSum += body.Transform - val.transform;
                    }

                    if (body.onlyWithStaticImpactCount > SeclConfig.ITERATIONS_FOR_STATIC_IMPACT_SLEEP)
                    {
                        if (body.withStaticImpactSum.position.Length() < SeclConfig.POSOFFSET_FOR_STATIC_IMPACT_SLEEP && 
                            body.withStaticImpactSum.orientation.MaxLength() < SeclConfig.MATOFFSET_FOR_STATIC_IMPACT_SLEEP)
                        {
                            body.isSleeping = true;
                        }
                        body.onlyWithStaticImpactCount = 0;
                        body.withStaticImpactSum = default;
                    }
                }                    
            }
            
            FreeManifolds();
            BroadPhase();
            UpdateCollision();
        }

        //note: collideDynamic isn't working now
        public void UpdateOne(SeclBody body, bool collideDynamic = false)
        {
            if (body.IsStatic || !body.IsSharedBody)
            {
                LogSystem.Warn("a static and a shared body don't need update one");
                return;
            }
            
            body.CollidedBodies.Clear();
            body.Update();
            dynamicBroadphase.UpdateBody(body);
            FreeManifolds();
            BroadPhase(body, collideDynamic);
            UpdateCollision();
        }

        /// <summary>
        /// Identifies collisions for a single body. Does not keep track of 
        /// symmetrical duplicates (they could be counted twice).
        /// </summary>
        private void BroadPhase(SeclBody query, bool collideDynamic = false)
        {
            SeclDebug.Assert(!query.IsStatic);

            reusableBuffer.Clear();
            staticBroadphase.QueryOverlap(query.AABB, reusableBuffer);
            if (collideDynamic)
                dynamicBroadphase.QueryOverlap(query.AABB, reusableBuffer);

            TestBuffer(query);
        }
        
        
        private void BroadPhase()
        {
            for (int i = 0; i < Bodies.Count; i++)
            {
                var query = Bodies[i];
        
                if (query.IsStatic)
                    continue;
                
                reusableBuffer.Clear();
                staticBroadphase.QueryOverlap(query.AABB, reusableBuffer);

                // HACK: Don't use dynamic broadphase for global updates for this.
                // It's faster if we do it manually because we can triangularize.
                for (int j = i + 1; j < Bodies.Count; j++)
                    if (!Bodies[j].IsStatic)
                        reusableBuffer.Add(Bodies[j]);

                TestBuffer(query);
            }
        }

        private void TestBuffer(SeclBody query)
        {
            for (int i = 0; i < reusableBuffer.Count; i++)
            {
                var test = reusableBuffer[i];                
                if(query == test)
                    continue;
                
                if (!query.AABB.Intersect(test.AABB))
                    continue;

                for (int j = 0; j < query.shapeCount; j++)
                    for (int k = 0; k < test.shapeCount; k++)
                        NarrowPhase(query.Shapes[j], test.Shapes[k]);
            }
        }

        private void NarrowPhase(
            SeclShape sa,
            SeclShape sb)
        {
            if (!sa.AABB.Intersect(sb.AABB))
                return;

            SeclShape.OrderShapes(ref sa, ref sb);
            Collision.Dispatch(this, sa, sb, manifolds);
        }

        private void UpdateCollision()
        {
            for (int i = 0; i < manifolds.Count; i++)
                manifolds[i].PrepareForIteration();

            for (int j = 0; j < 4; j++)
            {
                for (int i = 0; i < manifolds.Count; i++)
                    manifolds[i].Iterate();
            }
        }

        internal Manifold AllocateManifold()
        {
            return manifoldPool.Allocate();
        }
        
        private void FreeManifolds()
        {
            for (int i = 0; i < manifolds.Count; i++)
                manifoldPool.Deallocate(manifolds[i]);
            manifolds.Clear();
        }

        public void DebugDraw(IDebugDrawer d, DebugDrawFlags f)
        {
            if (f.HasFlag(DebugDrawFlags.DynamicBroadphaseTree))
                ((TreeBroadphase)staticBroadphase).DebugDraw(d, Color.red.WithAlpha(1));

            foreach (var m in manifolds)
            {
                d.SetColor(Color.orange);
                d.DrawSphere(m.p1, 0.3f);
                d.SetColor(Color.magenta);
                d.DrawSphere(m.p2, 0.3f);
                d.DrawLine(m.p2, m.p2 + m.normal);
            }
            
            for (int i = 0; i < Bodies.Count; i++)
            {
                var body = Bodies[i];
                if (body.IsStatic && f.HasFlag(DebugDrawFlags.StaticBodies) || !body.IsStatic && f.HasFlag(DebugDrawFlags.DynamicBodies))
                    body.DebugDraw(d, f);
            }
        }

        public void FreeShape(SeclShape shape)
        {
            shapesPools[shape.Type].Deallocate(shape);
        }
    }
}