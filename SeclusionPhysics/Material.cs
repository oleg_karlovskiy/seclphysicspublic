namespace SeclusionPhysics
{
    public struct Material
    {
        internal float kineticFriction;
        internal float staticFriction;
        internal float restitution;
    }
}