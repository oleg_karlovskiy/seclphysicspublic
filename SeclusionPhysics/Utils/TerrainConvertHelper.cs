namespace SeclusionPhysics.Utils
{
    public static class TerrainConvertHelper
    {
        public static void MulHeightMap(float[,] heights, float multiplier, float add)
        {
            var n0 = heights.GetLength(0);
            var n1 = heights.GetLength(1);
            for (int i = 0; i < n0; i++)
            {
                for (int j = 0; j < n1; j++)
                {
                    heights[i, j] = heights[i, j] * multiplier + add;
                }                
            }
        }

        public static float[,] Transpose(float[,] heights)
        {
            var n0 = heights.GetLength(0);
            var n1 = heights.GetLength(1);

            var ret = new float[n1, n0];
            
            for (int i = 0; i < n0; i++)
            {
                for (int j = 0; j < n1; j++)
                {
                    ret[j, i] = heights[i, j];
                }                
            }

            return ret;
        }
    }
}