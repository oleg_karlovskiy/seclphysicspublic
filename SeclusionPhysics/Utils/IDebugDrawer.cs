using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Utils
{
    public interface IDebugDrawer
    {
        void SetColor(Color color);
        void DrawLine(Vector3 from, Vector3 to);
        void DrawSphere(Vector3 center, float radius);
        void DrawBox(Transform t, Vector3 e);
    }
}