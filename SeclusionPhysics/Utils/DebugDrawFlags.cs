using System;

namespace SeclusionPhysics.Utils
{
    [Flags]
    public enum DebugDrawFlags
    {
        None = 0,
        DynamicBroadphaseTree = 2,
        DynamicBodies = 4,
        StaticBodies = 8,
        BodiesAABB = 16,
        DynamicBodiesHistory = 32
        
            
        //64, 128, 256, 512, 1024	
    }
}