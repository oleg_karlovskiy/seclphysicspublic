using System.Linq;
using UnityEngine;
using VionisEssencesSystem.GameCore.MiscStructs;
using Color = VionisEssencesSystem.GameCore.MiscStructs.Color;
using Transform = VionisEssencesSystem.GameCore.MiscStructs.Transform;
using Vector3 = VionisEssencesSystem.GameCore.MiscStructs.Vector3;
namespace SeclusionPhysics.Utils
{
    public class GizmoDebugDrawer : IDebugDrawer
    {
        public void SetColor(Color color)
        {
            Gizmos.color = color.ToUnity();
        }
        
        public void DrawLine(Vector3 from, Vector3 to)
        {
            Gizmos.DrawLine(from.ToUnity(), to.ToUnity());
        }

        public void DrawSphere(Vector3 center, float radius)
        {
            Gizmos.DrawSphere(center.ToUnity(), radius);
        }

        public void DrawBox(Transform t, Vector3 e)
        {
            Vector3[] vertices =
            {
                new Vector3(-e.x, -e.y, -e.z), //0
                new Vector3(-e.x, -e.y, e.z), //1
                new Vector3(-e.x, e.y, e.z), //2
                new Vector3(-e.x, e.y, -e.z), //3
                new Vector3(e.x, -e.y, -e.z), //4
                new Vector3(e.x, -e.y, e.z), //5
                new Vector3(e.x, e.y, e.z), //6
                new Vector3(e.x, e.y, -e.z) //7
            };

            vertices = vertices.ToList().Select(q => Transform.Mul(t, q)).ToArray();
            
            DrawLine(vertices[0], vertices[1]);
            DrawLine(vertices[1], vertices[2]);
            DrawLine(vertices[2], vertices[3]);
            DrawLine(vertices[3], vertices[0]);
            
            DrawLine(vertices[4], vertices[5]);
            DrawLine(vertices[5], vertices[6]);
            DrawLine(vertices[6], vertices[7]);
            DrawLine(vertices[7], vertices[4]);
            
            DrawLine(vertices[0], vertices[4]);
            DrawLine(vertices[1], vertices[5]);
            DrawLine(vertices[2], vertices[6]);
            DrawLine(vertices[3], vertices[7]);
        }
    }
}