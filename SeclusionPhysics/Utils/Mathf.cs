﻿using System;

namespace SeclusionPhysics.Utils
{
    public static class Mathf
    {
        public const float PI = 3.141593f;

        public static float Clamp(float value, float min, float max)
        {
            if (value > max)
                return max;
            if (value < min)
                return min;
            return value;
        }

        public static float Max(float a, float b)
        {
            if (a > b)
                return a;
            return b;
        }

        public static float Min(float a, float b)
        {
            if (a < b)
                return a;
            return b;
        }

        public static float Sqrt(float a)
        {
            return (float) System.Math.Sqrt(a);
        }

        public static float Sin(float a)
        {
            return (float) System.Math.Sin(a);
        }

        public static float Cos(float a)
        {
            return (float) System.Math.Cos(a);
        }

        public static float Atan2(float a, float b)
        {
            return (float) System.Math.Atan2(a, b);
        }

        public static float Sign(float f)
        {
            if (f < 0.0f)
                return -1;
            if (f > 0.0f)
                return 1;
            if (f == 0.0f)
                return 0;
            
            throw new ArithmeticException("Arithmetic_NaN");
        }

        public static float Clamp01(float value)
        {
            if (value < 0.0)
                return 0.0f;
            if (value > 1.0)
                return 1f;
            return value;
        }
    }
}