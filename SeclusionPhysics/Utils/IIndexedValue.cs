namespace SeclusionPhysics.Utils
{
    public interface IIndexedValue
    {
        int Index { get; set; }
    }
}