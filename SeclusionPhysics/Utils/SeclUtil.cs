using System;

namespace SeclusionPhysics.Utils
{
    public class SeclUtil
    {
        public static int ExpandArray<T>(ref T[] oldArray, int minSize = 1)
        {            
            int newCapacity = Math.Max(oldArray.Length * 2, minSize);
            T[] newArray = new T[newCapacity];
            Array.Copy(oldArray, newArray, oldArray.Length);
            oldArray = newArray;
            return newCapacity;
        }
    }
}