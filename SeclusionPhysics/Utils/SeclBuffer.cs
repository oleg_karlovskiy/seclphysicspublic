﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SeclusionPhysics.Utils
{
  public class SeclBuffer<T> : IEnumerable<T>
  {
    public int Count => _count;
    public T this[int key] => _items[key];

    private T[] _items;
    private int _count;

    public SeclBuffer(int capacity = 256)
    {
      _items = new T[capacity];
      _count = 0;
    }

    /// <summary>
    /// Adds a new element to the end of the list. Returns the index of the
    /// newly-indexed object.
    /// </summary>
    internal void Add(T body)
    {
      if (_count >= _items.Length)
        SeclUtil.ExpandArray(ref _items);

      _items[_count] = body;
      _count++;
    }

    internal void Add(T[] bodies, int count)
    {
      if (_count + count >= _items.Length)
        SeclUtil.ExpandArray(ref _items, _count + count);

      Array.Copy(bodies, 0, _items, _count, count);
      _count += count;
    }

    public void Clear()
    {
      _count = 0;
    }

    public IEnumerator<T> GetEnumerator()
    {
      for (int i = 0; i < _count; i++)
        yield return _items[i];
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      for (int i = 0; i < _count; i++)
        yield return _items[i];
    }
  }
}
