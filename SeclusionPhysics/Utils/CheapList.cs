using System.Collections;
using System.Collections.Generic;

namespace SeclusionPhysics.Utils
{
    public class CheapList<T> : IEnumerable<T>
        where T : class, IIndexedValue
    {
        public int Count => count;
        public T this[int key] => values[key];

        private T[] values;
        private int count;

        public CheapList(int capacity = 10)
        {
            values = new T[capacity];
            count = 0;
        }

        /// <summary>
        /// Adds a new element to the end of the list. Returns the index of the
        /// newly-indexed object.
        /// </summary>
        public void Add(T value)
        {
            if (count >= values.Length)
                SeclUtil.ExpandArray(ref values);

            values[count] = value;
            value.Index = count;
            count++;
        }

        /// <summary>
        /// Removes the element by swapping it for the last element in the list.
        /// </summary>
        public void Remove(T value)
        {
            int index = value.Index;
            SeclDebug.Assert(index >= 0);
            SeclDebug.Assert(index < count);

            int lastIndex = count - 1;
            if (index < lastIndex)
            {
                T lastValue = values[lastIndex];

                values[lastIndex].Index = -1;
                values[lastIndex] = null;

                values[index] = lastValue;
                lastValue.Index = index;
            }

            count--;
        }

        public void Clear()
        {
            count = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < count; i++)
                yield return values[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < count; i++)
                yield return values[i];
        }
    }    
}