using System;
using System.Diagnostics;
using Utils;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Utils
{
    public static class SeclDebug
    {
        [Conditional("DEBUG")]
        public static void Assert(bool condition)
        {
            if (condition == false)
                LogSystem.Err($"Assert Failed! {Environment.StackTrace}");
        }

        [Conditional("DEBUG")]
        public static void IsNormal(float d)
        {
            if (float.IsNaN(d))
                LogSystem.Err($"Assert Failed! {d} is NaN");
        }

        [Conditional("DEBUG")]
        public static void IsNormal(Vector3 d)
        {
            IsNormal(d.x);
            IsNormal(d.y);
            IsNormal(d.z);
        }

        [Conditional("DEBUG")]
        public static void IsNormal(Mat3 d)
        {
            IsNormal(d.ex);
            IsNormal(d.ey);
            IsNormal(d.ez);
        }
        
        [Conditional("DEBUG")]
        public static void Assert(bool condition, string message)
        {
            if (condition == false)
                LogSystem.Err($"Assert Failed({message})! {Environment.StackTrace}");
        }
    }
}