namespace SeclusionPhysics.Utils
{
    public interface IVoltPoolable<T>
        where T : IVoltPoolable<T>
    {
        ISeclPool<T> Pool { get; set; }
        void Reset();
    }           
}