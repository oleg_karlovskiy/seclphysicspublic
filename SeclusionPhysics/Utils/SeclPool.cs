﻿using System.Collections.Generic;

namespace SeclusionPhysics.Utils
{
  public interface ISeclPool<T>
  {
    T Allocate();
    void Deallocate(T obj);
    ISeclPool<T> Clone();
  }

  public class SeclPool
  {
    public static void Free<T>(T obj)
      where T : IVoltPoolable<T>
    {
      obj.Pool.Deallocate(obj);
    }

    public static void SafeReplace<T>(ref T destination, T obj)
      where T : IVoltPoolable<T>
    {
      if (destination != null)
        Free(destination);
      destination = obj;
    }

    public static void DrainQueue<T>(Queue<T> queue)
      where T : IVoltPoolable<T>
    {
      while (queue.Count > 0)
        Free(queue.Dequeue());
    }
  }

  internal abstract class SeclPoolBase<T> : ISeclPool<T>
    where T : IVoltPoolable<T>
  {
    private readonly Stack<T> freeList;

    public abstract ISeclPool<T> Clone();
    protected abstract T Create();

    public SeclPoolBase()
    {
      freeList = new Stack<T>();
    }

    public T Allocate()
    {
      T obj;
      if (freeList.Count > 0)
        obj = freeList.Pop();
      else
        obj = Create();

      obj.Pool = this;
      return obj;
    }

    public void Deallocate(T obj)
    {
      SeclDebug.Assert(obj.Pool == this);

      obj.Reset();
      obj.Pool = null; // Prevent multiple frees
      this.freeList.Push(obj);
    }
  }

  internal class SeclPool<T> : SeclPoolBase<T>
    where T : IVoltPoolable<T>, new()
  {
    protected override T Create()
    {
      return new T();
    }

    public override ISeclPool<T> Clone()
    {
      return new SeclPool<T>();
    }
  }

  internal class SeclPool<TBase, TDerived> : SeclPoolBase<TBase>
    where TBase : IVoltPoolable<TBase>
    where TDerived : TBase, new()
  {
    protected override TBase Create()
    {
      return new TDerived();
    }

    public override ISeclPool<TBase> Clone()
    {
      return new SeclPool<TBase, TDerived>();
    }
  }
}
