using System;
using System.Collections.Generic;
using SeclusionPhysics.Internals.Broadphase;
using SeclusionPhysics.Internals.History;
using SeclusionPhysics.Shapes;
using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;
using Mathf = SeclusionPhysics.Utils.Mathf;

namespace SeclusionPhysics
{
    public class SeclBody : IVoltPoolable<SeclBody>, IIndexedValue
    {
        public ISeclPool<SeclBody> Pool { get; set; }
        public int Index { get; set; }
        public int ProxyId { get; internal set; }
        public bool IsStatic { get; set; }
        public long Id { get; set; }

        /// <summary>
        /// We wont update body in common update, if this flag == true
        /// </summary>
        public bool IsSharedBody { get; set; }

        public bool IsParticle { get; set; }

        public readonly HashSet<SeclBody> CollidedBodies = new HashSet<SeclBody>();
        
        internal HistoryBuffer History;
        internal HistoryRecord currentState;

        internal float inverseMass;
        internal Mat3 inertia;
        internal Mat3 invInertia;

        internal Mat3 invInertiaWorld;
        internal Mat3 invOrientation;
        internal Vector3 linearVelocity;

        public void AddLinearForce(Vector3 v)
        {
            if (v != Vector3.zero)
            {                
                linearVelocity += v;
                isSleeping = false;
            }
        }
        
        internal Vector3 angularVelocity;
        
        internal bool isSleeping;
        internal bool onlyWithStaticImpact;
        internal bool inResimulation;
        internal int onlyWithStaticImpactCount;
        internal Transform withStaticImpactSum;
        
        
        internal Material material;

        public Transform Transform
        {
            get => currentState.transform;
            set => currentState.transform = value;
        }
        public VoltAABB AABB
        {
            get => currentState.aabb;
            set => currentState.aabb = value;
        }
        
        public SeclShape[] Shapes;
        internal int shapeCount;

        public SeclWorld World { get; private set; }
        public object UserObject { get; set; }
        public bool CollideContinuousApplied;

        public SeclBody()
        {
            Reset();
            ProxyId = TreeBroadphase.NULL_NODE;
        }
        
        public void Reset()
        {
            IsSharedBody = default;
            isSleeping = default;
            onlyWithStaticImpactCount = default;
            inResimulation = default;
            withStaticImpactSum = default;
            Id = -1;
            Index = -1;
            AABB = default;
            CollideContinuousApplied = default;
            World = null;
            Transform = Transform.Identity;
            inverseMass = 1.0f;
            inertia = default;
            invInertia = default;
    
            invInertiaWorld = default;
            invOrientation = default;
            linearVelocity = default;
            angularVelocity = default;
            
            material = default;
            History = null;
            currentState = new HistoryRecord {transform = Transform.Identity};
        }

        public void InitializeStatic(Transform t, SeclShape[] shapesToAdd)
        {
            Initialize(t, shapesToAdd);
            OnPositionUpdated();
            SetStatic();
            UpdateInternalMatrixs();
        }

        public void InitializeDynamic(Transform t, SeclShape[] shapesToAdd)
        {
            Initialize(t, shapesToAdd);
            OnPositionUpdated();
            SetDynamic();
        }

        private void SetStatic()
        {
            IsStatic = true;
        }

        private void SetDynamic()
        {
            IsStatic = false;
        }

        private void Initialize(Transform t, SeclShape[] shapesToAdd)
        {
            Transform = t;

            material = new Material
            {
                kineticFriction = 0.3f,
                staticFriction = 1f,
                restitution = 0.0f
            };

            if (Shapes == null || Shapes.Length < shapesToAdd.Length)
                Shapes = new SeclShape[shapesToAdd.Length];
            Array.Copy(shapesToAdd, Shapes, shapesToAdd.Length);
            shapeCount = shapesToAdd.Length;
            for (int i = 0; i < shapeCount; i++)
                Shapes[i].AssignBody(this);
            
            SetMassProperties();
        }


        public void ResimulateSetup(Transform t)
        {
            currentState.transform = t;
            inResimulation = true;
        }
        
        public void ContinueResimulate(Vector3 linearVelocity, Vector3 angularVelocity)
        {
            this.linearVelocity = linearVelocity;
            this.angularVelocity = angularVelocity;
            World.UpdateOne(this);
        }

        public void ResimulateFinish()
        {
            inResimulation = false;
//            History.ReplaceCurrent(currentState);
        }
        
        public void SetMassProperties()
        {
            var sh = Shapes[0];
            inertia = sh.inertia;
            Mat3.Inverse(ref inertia, out invInertia);
            inverseMass = 1.0f / sh.mass;
        }

        public void Update()
        {
            if (History != null && !inResimulation)
                History.Store(currentState);
            Integrate(true);
            OnPositionUpdated();
        }

        public void Integrate(bool damping)
        {
            if (IsStatic || isSleeping)
                return;
            var timestep = SeclConfig.TARGET_TIMESTEP;
            Vector3.Multiply(ref linearVelocity, timestep, out var temp);
            Vector3.Add(ref temp, ref currentState.transform.position, out currentState.transform.position);

            if (!IsParticle)
            {
                float angle = angularVelocity.Length();

                Vector3 axis;

                if (angle < 0.001f)
                {
                    // use Taylor's expansions of sync function
                    // axis = body.angularVelocity * (0.5f * timestep - (timestep * timestep * timestep) * (0.020833333333f) * angle * angle);
                    Vector3.Multiply(ref angularVelocity, 0.5f * timestep - timestep * timestep * timestep * 0.020833333333f * angle * angle, out axis);
                }
                else
                {
                    // sync(fAngle) = sin(c*fAngle)/t
                    Vector3.Multiply(ref angularVelocity, (float)Math.Sin(0.5f * angle * timestep) / angle, out axis);
                }

                var dorn = new Quaternion(axis.x, axis.y, axis.z, (float)Math.Cos(angle * timestep * 0.5f));
                Quaternion ornA; Quaternion.CreateFromMatrix(ref currentState.transform.orientation, out ornA);

                Quaternion.Multiply(ref dorn, ref ornA, out dorn);

                dorn.Normalize(); Mat3.CreateFromQuaternion(ref dorn, out currentState.transform.orientation);
            }

            if (damping)
            {
                Vector3.Multiply(ref linearVelocity, SeclConfig.DEFAULT_LINEAR_DAMPING, out linearVelocity);
                Vector3.Multiply(ref angularVelocity, SeclConfig.DEFAULT_ANGULAR_DAMPING, out angularVelocity);
                Vector3.Multiply(ref World.Gravity, timestep, out temp);
                Vector3.Add(ref linearVelocity, ref temp, out linearVelocity);
            }
            
            UpdateInternalMatrixs();
            Mat3.Multiply(ref invOrientation, ref invInertia, out invInertiaWorld);
            Mat3.Multiply(ref invInertiaWorld, ref currentState.transform.orientation, out invInertiaWorld);

            linearVelocity.Stabilize();
            angularVelocity.Stabilize();
            currentState.Stabilize();
        }

        public void UpdateInternalMatrixs()
        {
            Mat3.Transpose(ref currentState.transform.orientation, out invOrientation);

        }

        private void OnPositionUpdated()
        {
            for (int i = 0; i < shapeCount; i++)
                Shapes[i].OnBodyPositionUpdated();
            UpdateAABB();
        }

        private void UpdateAABB()
        {
            var min = new Vector3(float.MaxValue, float.MaxValue,float.MaxValue);
            var max = new Vector3(float.MinValue, float.MinValue,float.MinValue);            

            for (int i = 0; i < shapeCount; i++)
            {
                var aabb = Shapes[i].AABB;
                min.x = Mathf.Min(min.x, aabb.Min.x);
                min.y = Mathf.Min(min.y, aabb.Min.y);
                min.z = Mathf.Min(min.z, aabb.Min.z);
                
                max.x = Mathf.Max(max.x, aabb.Max.x);
                max.y = Mathf.Max(max.y, aabb.Max.y);
                max.z = Mathf.Max(max.z, aabb.Max.z);                
            }

            AABB = new VoltAABB(min.x, min.y, min.z, max.x, max.y, max.z);
        }

        internal void AssignHistory(HistoryBuffer history)
        {
            SeclDebug.Assert(!IsStatic);
            History = history;
        }

        internal void AssignWorld(SeclWorld world)
        {
            World = world;
        }

        internal void FreeShapes()
        {
            if (World != null)
            {
                for (int i = 0; i < shapeCount; i++)
                    World.FreeShape(Shapes[i]);
                for (int i = 0; i < Shapes.Length; i++)
                    Shapes[i] = null;
            }
            shapeCount = 0;
        }

        public void FreeHistory()
        {
            if (World != null && History != null)
                World.FreeHistory(History);
            History = null;
        }

        public void DebugDraw(IDebugDrawer d, DebugDrawFlags f)
        {
            for (int i = 0; i < shapeCount; i++)
            {
                Shapes[i].DebugDraw(d, IsStatic ? Color.blue: Color.orange);
            }
            
            if (f.HasFlag(DebugDrawFlags.BodiesAABB))
            {
                d.SetColor(Color.cyan);
                var e = (AABB.Max - AABB.Min) / 2;
                d.DrawBox(new Transform
                    {
                        position = AABB.Min + e,
                        orientation = Mat3.Identity
                    }, e);
            }

            if (f.HasFlag(DebugDrawFlags.DynamicBodiesHistory) && History != null)
            {                
                foreach (HistoryRecord record in History.GetValues())
                    record.aabb.DebugDraw(d, Color.cyan.WithAlpha(0.6f));
            }
        }
    }
}