﻿using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Internals.Broadphase
{
    internal class NaiveBroadphase : IBroadPhase
    {
        private SeclBody[] _bodies;
        private int _count;

        public NaiveBroadphase()
        {
            _bodies = new SeclBody[256];
            _count = 0;
        }

        public void AddBody(SeclBody body)
        {
            if (_count >= _bodies.Length)
                SeclUtil.ExpandArray(ref _bodies);

            _bodies[_count] = body;
            body.ProxyId = _count;
            _count++;
        }

        public void RemoveBody(SeclBody body)
        {
            var index = body.ProxyId;
            SeclDebug.Assert(index >= 0);
            SeclDebug.Assert(index < _count);

            var lastIndex = _count - 1;
            if (index < lastIndex)
            {
                var lastBody = _bodies[lastIndex];

                _bodies[lastIndex].ProxyId = -1;
                _bodies[lastIndex] = null;

                _bodies[index] = lastBody;
                lastBody.ProxyId = index;
            }

            body.ProxyId = TreeBroadphase.NULL_NODE;
            _count--;
        }

        public void UpdateBody(SeclBody body)
        {
            // Do nothing
        }

        public void QueryOverlap(VoltAABB aabb, SeclBuffer<SeclBody> outBuffer)
        {
            outBuffer.Add(_bodies, _count);
        }

        public void QueryPoint(Vector3 point, SeclBuffer<SeclBody> outBuffer)
        {
            outBuffer.Add(_bodies, _count);
        }
    }
}