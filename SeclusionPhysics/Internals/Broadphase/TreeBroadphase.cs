﻿/*
 *  VolatilePhysics - A 2D Physics Library for Networked Games
 *  Copyright (c) 2015-2016 - Alexander Shoulson - http://ashoulson.com
 *  Some improvements and fixes from Oleg Karlovskiy
 *
 *  This software is provided 'as-is', without any express or implied
 *  warranty. In no event will the authors be held liable for any damages
 *  arising from the use of this software.
 *  Permission is granted to anyone to use this software for any purpose,
 *  including commercial applications, and to alter it and redistribute it
 *  freely, subject to the following restrictions:
 *  
 *  1. The origin of this software must not be misrepresented; you must not
 *     claim that you wrote the original software. If you use this software
 *     in a product, an acknowledgment in the product documentation would be
 *     appreciated but is not required.
 *  2. Altered source versions must be plainly marked as such, and must not be
 *     misrepresented as being the original software.
 *  3. This notice may not be removed or altered from any source distribution.
*/

/*
* Farseer Physics Engine:
* Copyright (c) 2012 Ian Qvist
* 
* Original source Box2D:
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org 
* 
* This software is provided 'as-is', without any express or implied 
* warranty.  In no event will the authors be held liable for any damages 
* arising from the use of this software. 
* Permission is granted to anyone to use this software for any purpose, 
* including commercial applications, and to alter it and redistribute it 
* freely, subject to the following restrictions: 
* 1. The origin of this software must not be misrepresented; you must not 
* claim that you wrote the original software. If you use this software 
* in a product, an acknowledgment in the product documentation would be 
* appreciated but is not required. 
* 2. Altered source versions must be plainly marked as such, and must not be 
* misrepresented as being the original software. 
* 3. This notice may not be removed or altered from any source distribution. 
*/

using System;
using System.Collections.Generic;
using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Internals.Broadphase
{
    /// <summary>
    /// A dynamic tree bounding volume hierarchy used for collision detection.
    /// Does not support raycasts (for now) as they would not be compatible with
    /// Volatile's historical raycast capability -- no past-step data is preserved
    /// in this tree.
    /// 
    /// Bounding AABBs are expanded to allow for movement room according to
    /// the VoltConfig.AABB_EXTENSION setting.
    ///
    /// Nodes are pooled and relocatable, so we use node indices rather than pointers.
    /// </summary>
    internal class TreeBroadphase : IBroadPhase
    {
        private delegate bool AABBTest(ref VoltAABB aabb);

        internal const int NULL_NODE = -1;

        /// <summary>
        /// A node in the dynamic tree.
        /// </summary>
        private class Node
        {
            internal bool IsLeaf => left == NULL_NODE;

            /// <summary>
            /// Expanded AABB.
            /// </summary>
            internal VoltAABB aabb;

            internal int left;
            internal int right;
            internal int height;
            internal int parentOrNext;
            internal SeclBody body;

            internal Node()
            {
                Reset();
            }

            internal Node(int parentOrNext, int height)
            {
                this.parentOrNext = parentOrNext;
                this.height = height;
            }

            internal void Initialize(int parentOrNext, int height)
            {
                this.parentOrNext = parentOrNext;
                this.height = height;
            }

            internal void Reset()
            {
                aabb = default(VoltAABB);
                left = NULL_NODE;
                right = NULL_NODE;
                height = 0;
                parentOrNext = NULL_NODE;
                body = null;
            }
        }

        private readonly Stack<int> queryStack;

        private int freeList;
        private int nodeCapacity;
        private int nodeCount;
        private Node[] nodes;
        private int rootId;

        public TreeBroadphase()
        {
            rootId = NULL_NODE;

            queryStack = new Stack<int>(256);

            nodeCapacity = 16;
            nodeCount = 0;
            nodes = new Node[nodeCapacity];

            // Build a linked list for the free list
            for (int i = 0; i < nodeCapacity - 1; ++i)
                nodes[i] = new Node(i + 1, 1);
            nodes[nodeCapacity - 1] = new Node(NULL_NODE, 1);
            freeList = 0;
        }

        /// <summary>
        /// Compute the height of the binary tree in O(N) time.
        /// Should not be called often.
        /// </summary>
        public int Height
        {
            get
            {
                if (rootId == NULL_NODE)
                    return 0;
                return nodes[rootId].height;
            }
        }

        /// <summary>
        /// Get the ratio of the sum of the node areas to the root area.
        /// </summary>
        public float AreaRatio
        {
            get
            {
                if (rootId == NULL_NODE)
                    return 0.0f;

                Node root = nodes[rootId];
                float rootArea = root.aabb.Perimeter;

                float totalArea = 0.0f;
                for (int i = 0; i < nodeCapacity; ++i)
                {
                    Node node = nodes[i];
                    if (node.height < 0)
                        continue;
                    totalArea += node.aabb.Perimeter;
                }

                return totalArea / rootArea;
            }
        }

        /// <summary>
        /// Get the maximum balance of an node in the tree. The balance is the
        /// difference in height of the two children of a node.
        /// </summary>
        public int MaxBalance
        {
            get
            {
                int maxBalance = 0;
                for (int i = 0; i < nodeCapacity; ++i)
                {
                    Node node = nodes[i];
                    if (node.height <= 1)
                        continue;

                    SeclDebug.Assert(!node.IsLeaf);
                    int balance =
                        Math.Abs(
                            nodes[node.right].height -
                            nodes[node.left].height);

                    maxBalance = Math.Max(maxBalance, balance);
                }

                return maxBalance;
            }
        }

        /// <summary>
        /// Compute the height of the entire tree.
        /// </summary>
        public int ComputeHeight()
        {
            return ComputeHeight(rootId);
        }

        /// <summary>
        /// Adds a body to the tree.     
        /// </summary>
        public void AddBody(SeclBody body)
        {
            SeclDebug.Assert(body.ProxyId == NULL_NODE);

            int proxyId;
            Node proxyNode = AllocateNode(out proxyId);

            // Expand the aabb
            proxyNode.aabb =
                VoltAABB.CreateExpanded(
                    body.AABB,
                    SeclConfig.AABB_EXTENSION);

            proxyNode.body = body;
            proxyNode.height = 0;

            InsertLeaf(proxyId);
            body.ProxyId = proxyId;
        }

        /// <summary>
        /// Removes a body from the tree.
        /// </summary>
        public void RemoveBody(SeclBody body)
        {
            int proxyId = body.ProxyId;
            SeclDebug.Assert(0 <= proxyId && proxyId < nodeCapacity);
            SeclDebug.Assert(nodes[proxyId].IsLeaf);

            RemoveLeaf(proxyId);
            FreeNode(proxyId);

            body.ProxyId = NULL_NODE;
        }

        /// <summary>
        /// Updates a body's position. If the body has moved outside of its
        /// expanded AABB, then the body is removed from the tree and re-inserted.
        /// Otherwise the function returns immediately.
        /// </summary>
        public void UpdateBody(SeclBody body)
        {
            int proxyId = body.ProxyId;
            SeclDebug.Assert(0 <= proxyId && proxyId < nodeCapacity);

            Node proxyNode = nodes[proxyId];
            SeclDebug.Assert(proxyNode.IsLeaf);

            if (proxyNode.aabb.Contains(body.AABB))
                return;
            RemoveLeaf(proxyId);

            // Extend AABB
            VoltAABB expanded =
                VoltAABB.CreateExpanded(body.AABB, SeclConfig.AABB_EXTENSION);

            // Predict AABB displacement and sweep the AABB
            //Vector2 sweep = VoltConfig.AABB_MULTIPLIER * displacement;
            //VoltAABB swept = VoltAABB.CreateSwept(expanded, sweep);
            //this.nodes[proxyId].aabb = swept;

            proxyNode.aabb = expanded;
            InsertLeaf(proxyId);
        }

        #region Tests

        public void QueryOverlap(
            VoltAABB aabb,
            SeclBuffer<SeclBody> outBuffer)
        {
            StartQuery(outBuffer);
            while (queryStack.Count > 0)
            {
                Node node = GetNextNode();
                if (node.aabb.Intersect(aabb))
                    ExpandNode(node, outBuffer);
            }
        }

        public void QueryPoint(
            Vector3 point,
            SeclBuffer<SeclBody> outBuffer)
        {
            StartQuery(outBuffer);
            while (queryStack.Count > 0)
            {
                Node node = GetNextNode();
                if (node.aabb.QueryPoint(point))
                    ExpandNode(node, outBuffer);
            }
        }



        #endregion

        #region Internals

        #region Query Internals

        private void StartQuery(SeclBuffer<SeclBody> outBuffer)
        {
            queryStack.Clear();
            ExpandChild(rootId, outBuffer);
        }

        private Node GetNextNode()
        {
            return nodes[queryStack.Pop()];
        }

        private void ExpandNode(Node node, SeclBuffer<SeclBody> outBuffer)
        {
            SeclDebug.Assert(!node.IsLeaf);
            ExpandChild(node.left, outBuffer);
            ExpandChild(node.right, outBuffer);
        }

        /// <summary>
        /// If the node is a leaf, we do not test the actual proxy bounding box.
        /// This is redundant since we will be testing the body's bounding box in
        /// the first step of the narrowphase, and the two are almost equivalent.
        /// </summary>
        private void ExpandChild(int query, SeclBuffer<SeclBody> outBuffer)
        {
            if (query != NULL_NODE)
            {
                Node node = nodes[query];
                if (node.IsLeaf)
                    outBuffer.Add(node.body);
                else
                    queryStack.Push(query);
            }
        }

        #endregion

        private Node AllocateNode(out int nodeId)
        {
            // Expand the node pool as needed
            if (freeList == NULL_NODE)
            {
                SeclDebug.Assert(nodeCount == nodeCapacity);

                // The free list is empty -- rebuild a bigger pool
                Node[] oldNodes = nodes;
                nodeCapacity = SeclUtil.ExpandArray(ref nodes);
                Array.Copy(oldNodes, nodes, nodeCount);

                // Build a linked list for the free list
                // The parent pointer becomes the "next" pointer
                for (int i = nodeCount; i < nodeCapacity - 1; ++i)
                    nodes[i] = new Node(i + 1, -1);

                nodes[nodeCapacity - 1] = new Node(NULL_NODE, -1);
                freeList = nodeCount;
            }

            // Peel a node off the free list.
            nodeId = freeList;
            Node result = nodes[nodeId];

            freeList = result.parentOrNext;
            result.Reset();

            nodeCount++;
            return result;
        }

        private void FreeNode(int nodeId)
        {
            SeclDebug.Assert(0 <= nodeId && nodeId < nodeCapacity);
            SeclDebug.Assert(0 < nodeCount);

            nodes[nodeId].Initialize(freeList, -1);
            freeList = nodeId;

            nodeCount--;
        }

        private void InsertLeaf(int leafId)
        {
            if (rootId == NULL_NODE)
            {
                rootId = leafId;
                nodes[rootId].parentOrNext = NULL_NODE;
                return;
            }

            // Find the best sibling for this node
            Node leafNode = nodes[leafId];
            VoltAABB leafAABB = leafNode.aabb;
            int siblingId = FindBestSibling(ref leafAABB);
            Node sibling = nodes[siblingId];

            // Create a new parent
            int oldParentId = sibling.parentOrNext;
            int newParentId;

            Node newParent = AllocateNode(out newParentId);
            newParent.Initialize(oldParentId, sibling.height + 1);
            newParent.aabb = VoltAABB.CreateMerged(leafAABB, sibling.aabb);

            if (oldParentId != NULL_NODE)
            {
                Node oldParent = nodes[oldParentId];
                // The sibling was not the root
                if (oldParent.left == siblingId)
                    oldParent.left = newParentId;
                else
                    oldParent.right = newParentId;
            }
            else
            {
                // The sibling was the root
                rootId = newParentId;
            }

            newParent.left = siblingId;
            newParent.right = leafId;
            sibling.parentOrNext = newParentId;
            leafNode.parentOrNext = newParentId;

            // Walk back up the tree fixing heights and AABBs
            FixAncestors(leafNode.parentOrNext);
        }

        private int FindBestSibling(ref VoltAABB leafAABB)
        {
            int index = rootId;
            while (!nodes[index].IsLeaf)
            {
                Node indexNode = nodes[index];

                int child1 = indexNode.left;
                int child2 = indexNode.right;

                float area = indexNode.aabb.Perimeter;

                VoltAABB.CreateMerged(indexNode.aabb, leafAABB);

                // Minimum cost of pushing the leaf further down the tree
                float inheritanceCost = 2.0f * -area;
                float cost1 = GetCost(child1, ref leafAABB) + inheritanceCost;
                float cost2 = GetCost(child2, ref leafAABB) + inheritanceCost;

                // Descend according to the minimum cost.
                if (cost1 < cost2)
                    break;

                // Descend
                if (cost1 < cost2)
                    index = child1;
                else
                    index = child2;
            }

            return index;
        }

        private void FixAncestors(int index)
        {
            while (index != NULL_NODE)
            {
                index = Balance(index);

                Node indexNode = nodes[index];
                Node left = nodes[indexNode.left];
                Node right = nodes[indexNode.right];

                indexNode.aabb = VoltAABB.CreateMerged(left.aabb, right.aabb);
                indexNode.height = 1 + Math.Max(left.height, right.height);
                index = indexNode.parentOrNext;
            }
        }

        private float GetCost(int index, ref VoltAABB leafAABB)
        {
            if (nodes[index].IsLeaf)
            {
                VoltAABB aabb =
                    VoltAABB.CreateMerged(leafAABB, nodes[index].aabb);
                return aabb.Perimeter;
            }
            else
            {
                VoltAABB aabb =
                    VoltAABB.CreateMerged(leafAABB, nodes[index].aabb);
                float oldArea = nodes[index].aabb.Perimeter;
                float newArea = aabb.Perimeter;
                return newArea - oldArea;
            }
        }

        private void RemoveLeaf(int leafId)
        {
            if (leafId == rootId)
            {
                rootId = NULL_NODE;
                return;
            }

            int parentId = nodes[leafId].parentOrNext;
            Node parent = nodes[parentId];

            int siblingId = parent.left == leafId ? parent.right : parent.left;
            Node sibling = nodes[siblingId];

            int grandParentId = parent.parentOrNext;
            if (grandParentId != NULL_NODE)
            {
                // Destroy parent and connect sibling to grandParent
                Node grandparent = nodes[grandParentId];
                if (grandparent.left == parentId)
                    grandparent.left = siblingId;
                else
                    grandparent.right = siblingId;
                sibling.parentOrNext = grandParentId;
                FreeNode(parentId);

                // Fix the tree going up
                FixAncestors(grandParentId);
            }
            else
            {
                rootId = siblingId;
                sibling.parentOrNext = NULL_NODE;
                FreeNode(parentId);
            }
        }

        /// <summary>
        /// Perform a left or right rotation if node A is imbalanced.
        /// </summary>
        private int Balance(int iA)
        {
            SeclDebug.Assert(iA != NULL_NODE);

            Node A = nodes[iA];
            if (A.IsLeaf || A.height < 2)
                return iA;

            int iB = A.left;
            int iC = A.right;
            SeclDebug.Assert(0 <= iB && iB < nodeCapacity);
            SeclDebug.Assert(0 <= iC && iC < nodeCapacity);

            Node B = nodes[iB];
            Node C = nodes[iC];
            int balance = C.height - B.height;

            if (balance > 1) // Rotate C up
                return Rotate(A, B, C, iA, iC, false);
            if (balance < -1) // Rotate B up
                return Rotate(A, C, B, iA, iB, true);
            return iA;
        }

        private int Rotate(
            Node P,
            Node Q,
            Node R,
            int iP,
            int iR,
            bool left)
        {
            int iX = R.left;
            int iY = R.right;
            Node X = nodes[iX];
            Node Y = nodes[iY];
            SeclDebug.Assert(0 <= iX && iX < nodeCapacity);
            SeclDebug.Assert(0 <= iY && iY < nodeCapacity);

            // Swap P and R
            R.left = iP;
            R.parentOrNext = P.parentOrNext;
            P.parentOrNext = iR;

            // P's old parent should point to R
            if (R.parentOrNext != NULL_NODE)
            {
                if (nodes[R.parentOrNext].left == iP)
                {
                    nodes[R.parentOrNext].left = iR;
                }
                else
                {
                    SeclDebug.Assert(nodes[R.parentOrNext].right == iP);
                    nodes[R.parentOrNext].right = iR;
                }
            }
            else
            {
                rootId = iR;
            }

            // Rotate
            if (X.height > Y.height)
                UpdateRotated(P, Q, R, iP, iX, iY, left);
            else
                UpdateRotated(P, Q, R, iP, iY, iX, left);

            return iR;
        }

        private void UpdateRotated(
            Node P,
            Node Q,
            Node R,
            int iP,
            int iX,
            int iY,
            bool left)
        {
            Node X = nodes[iX];
            Node Y = nodes[iY];

            R.right = iX;
            if (left)
                P.left = iY;
            else
                P.right = iY;

            Y.parentOrNext = iP;
            P.aabb = VoltAABB.CreateMerged(Q.aabb, Y.aabb);
            R.aabb = VoltAABB.CreateMerged(P.aabb, X.aabb);

            P.height = 1 + Math.Max(Q.height, Y.height);
            R.height = 1 + Math.Max(P.height, X.height);
        }

        /// <summary>
        /// Compute the height of a sub-tree.
        /// </summary>
        private int ComputeHeight(int nodeId)
        {
            SeclDebug.Assert(0 <= nodeId && nodeId < nodeCapacity);
            var node = nodes[nodeId];
            if (node.IsLeaf)
                return 0;

            var height1 = ComputeHeight(node.left);
            var height2 = ComputeHeight(node.right);
            return 1 + Math.Max(height1, height2);
        }

        #endregion
        
        public void DebugDraw(IDebugDrawer d, Color c)
        {
            DoDebugDraw(d, c, rootId, 0);
        }

        private void DoDebugDraw(IDebugDrawer d, Color c, int nodeId, int lvl)
        {
            if (nodeId == NULL_NODE)
                return;

            var node = nodes[nodeId];
                        
            node.aabb.DebugDraw(d, c);

            DoDebugDraw(d, c, node.left, lvl + 1);
            DoDebugDraw(d, c, node.right, lvl + 1);
        }
    }
}