﻿using SeclusionPhysics.Structs;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Internals.Broadphase
{
    internal interface IBroadPhase
    {
        void AddBody(SeclBody body);
        void RemoveBody(SeclBody body);
        void UpdateBody(SeclBody body);

        // Note that these should return bodies that meet the criteria within the
        // spaces defined by the structure itself. These tests should not test the
        // actual body's bounding box, as that will happen in the beginning of the
        // narrowphase test.
        void QueryOverlap(VoltAABB aabb, SeclBuffer<SeclBody> outBuffer);
        void QueryPoint(Vector3 point, SeclBuffer<SeclBody> outBuffer);
    }
}