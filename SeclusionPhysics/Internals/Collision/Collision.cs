﻿using System.Collections.Generic;
using SeclusionPhysics.Shapes;
using SeclusionPhysics.Utils;
using Vector3 = VionisEssencesSystem.GameCore.MiscStructs.Vector3;

namespace SeclusionPhysics.Internals.Collision
{
    public static class Collision
    {
        private const bool UseTerrainNormal = true;

        internal static void Dispatch(SeclWorld world, SeclShape s1, SeclShape s2, List<Manifold> manifolds)
        {
            bool withStatic = s1.Body.IsStatic || s2.Body.IsStatic;
            bool isCollided;
            float penetration;
            Vector3 point1;
            Vector3 point2;
            Vector3 normal; //TODO: maybe optimize ?
            
            var b1IsMulti = s1 is Multishape;
            var b2IsMulti = s2 is Multishape;

            Vector3 point;

            if (!b1IsMulti && !b2IsMulti)
            {
                isCollided = XenoCollide.Detect(s1, s2, ref s1.Body.currentState.transform.orientation,
                    ref s2.Body.currentState.transform.orientation,
                    ref s1.Body.currentState.transform.position, ref s2.Body.currentState.transform.position, out point,
                    out normal, out penetration);
                
                penetration = Mathf.Clamp01(penetration);
                normal.Normalize();

                if (!isCollided)                                    
                    return;

                s1.Body.CollidedBodies.Add(s2.Body);
                s2.Body.CollidedBodies.Add(s1.Body);
            
                FindSupportPoints(s1, s2, ref point, ref normal, out point1, out point2);
                if (!withStatic)
                {
                    s1.Body.onlyWithStaticImpact = false;
                    s2.Body.onlyWithStaticImpact = false;
                
                    s1.Body.isSleeping = false;
                    s2.Body.isSleeping = false;
                }
                
                SeclDebug.IsNormal(normal);
                SeclDebug.IsNormal(penetration);
                SeclDebug.IsNormal(point1);
                SeclDebug.IsNormal(point2);
                manifolds.Add(world.AllocateManifold().Assign(world, s1, s2, withStatic, point1, point2, normal, penetration, true));
            }
            else if ((b1IsMulti && !b2IsMulti) || (!b1IsMulti && b2IsMulti))
            {
                SeclShape b1, b2;

                if (s2 is Multishape) { b1 = s2; b2 = s1; }
                else { b2 = s2; b1 = s1; }

                Multishape ms = (b1 as Multishape);

                ms = ms.RequestWorkingClone();

                var transformedBoundingBox = b2.worldSpaceAABB;
                transformedBoundingBox.InverseTransform(ref ms.Body.currentState.transform.position, ref ms.Body.currentState.transform.orientation);

                int msLength = ms.Prepare(ref transformedBoundingBox);
                var isFirstCollided = false;
                for (int i = 0; i < msLength; i++)
                {
//                    //71
//                    //((-8.07, -74.44, -23.23) ((-0.51, 0.05, -0.86), (0.77, 0.48, -0.43), (0.39, -0.88, -0.28)))
//
//                    if (b2.Body.Transform.position.ApproximateEqual(new Vector3(-8.07f, -74.44f, -23.23f)) &&
//                        b2.Body.Transform.orientation.ApproximateEqual(new Mat3(new Vector3(-0.51f, 0.05f, -0.86f),
//                            new Vector3(0.77f, 0.48f, -0.43f),
//                            new Vector3(0.39f, -0.88f, -0.28f)))
//                        && i == 71)
//                    {
//                        
//                    }
                    
                    
                    ms.SetCurrentShape(i);
                
                    isCollided = XenoCollide.Detect(ms, b2, ref ms.Body.currentState.transform.orientation,
                        ref b2.Body.currentState.transform.orientation, ref ms.Body.currentState.transform.position,
                        ref b2.Body.currentState.transform.position,
                        out point, out normal, out penetration);

                    penetration = Mathf.Clamp01(penetration);
                    normal.Normalize();

                    if (isCollided)
                    {
                        if (!isFirstCollided)
                        {
                            isFirstCollided = true;
                            ms.Body.CollidedBodies.Add(b2.Body);
                            b2.Body.CollidedBodies.Add(ms.Body);
                        }
                        
                        FindSupportPoints(ms, b2, ref point, ref normal, out point1, out point2);

                        if (UseTerrainNormal && ms is TerrainShape shape)
                        {
                            shape.CollisionNormal(out normal);
                            Vector3.Transform(ref normal, ref ms.Body.currentState.transform.orientation, out normal);
                        }
                        
                        SeclDebug.IsNormal(normal);
                        SeclDebug.IsNormal(penetration);
                        SeclDebug.IsNormal(point1);
                        SeclDebug.IsNormal(point2);
                        manifolds.Add(world.AllocateManifold().Assign(world, ms, b2, withStatic, point1, point2, normal, penetration, true));
                    }
                }

                ms.ReturnWorkingClone();
            }
            else
            {
                //not implemented colliding two Multishapes
            }
        }

        private static void FindSupportPoints(SeclShape shape1, SeclShape shape2, ref Vector3 point, ref Vector3 normal, out Vector3 point1, out Vector3 point2)
        {
            Vector3 mn; Vector3.Negate(ref normal, out mn);

            Vector3 sA; SupportMapping(shape1, ref mn, out sA);
            Vector3 sB; SupportMapping(shape2, ref normal, out sB);

            Vector3.Subtract(ref sA, ref point, out sA);
            Vector3.Subtract(ref sB, ref point, out sB);

            float dot1 = Vector3.Dot(ref sA, ref normal);
            float dot2 = Vector3.Dot(ref sB, ref normal);

            Vector3.Multiply(ref normal, dot1, out sA);
            Vector3.Multiply(ref normal, dot2, out sB);

            Vector3.Add(ref point, ref sA, out point1);
            Vector3.Add(ref point, ref sB, out point2);
        }

        private static void SupportMapping(SeclShape workingShape, ref Vector3 direction, out Vector3 result)
        {
            var body = workingShape.Body;
            Vector3.Transform(ref direction, ref body.invOrientation, out result);
            workingShape.SupportMapping(ref result, out result);
            Vector3.Transform(ref result, ref body.currentState.transform.orientation, out result);
            Vector3.Add(ref result, ref body.currentState.transform.position, out result);
        }
    }
}