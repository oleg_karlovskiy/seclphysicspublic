﻿using SeclusionPhysics.Shapes;
using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;
using Mathf = SeclusionPhysics.Utils.Mathf;

namespace SeclusionPhysics.Internals.Collision
{
    internal sealed class Manifold
        : IVoltPoolable<Manifold>
    {
        #region Interface

        ISeclPool<Manifold> IVoltPoolable<Manifold>.Pool { get; set; }

        void IVoltPoolable<Manifold>.Reset()
        {
            Reset();
        }

        #endregion
        
        bool body1IsMassPoint; bool body2IsMassPoint;
        
        internal SeclShape ShapeA { get; private set; }
        internal SeclShape ShapeB { get; private set; }
        internal bool WithStatic { get; private set; }

        private SeclWorld world;
        
        private bool newContact = false;
        internal float penetration = 0.0f;
        internal float initialPen = 0.0f;
        
        internal Vector3 normal, tangent;

        internal Vector3 realRelPos1, realRelPos2;
        internal Vector3 relativePos1, relativePos2;
        internal Vector3 p1, p2;

        internal float accumulatedNormalImpulse = 0.0f;
        internal float accumulatedTangentImpulse = 0.0f;
        private float staticFriction, dynamicFriction, restitution;
        private float friction = 0.0f;
        private float massNormal = 0.0f, massTangent = 0.0f;
        private float restitutionBias = 0.0f;
        
        private bool treatBody1AsStatic = false;
        private bool treatBody2AsStatic = false;
        
        float lostSpeculativeBounce = 0.0f;
        float speculativeVelocity = 0.0f;

        private bool liteTest;

        float lastTimeStep;

        private ContactSettings settings = new ContactSettings();


        public Manifold()
        {
            Reset();
        }

        internal Manifold Assign(SeclWorld world,
            SeclShape shapeA,
            SeclShape shapeB)
        {
            this.world = world;

            ShapeA = shapeA;
            ShapeB = shapeB;
            liteTest = true;
            return this;
        }

        internal Manifold Assign(SeclWorld world,
            SeclShape shapeA,
            SeclShape shapeB,
            bool withStatic, Vector3 point1, Vector3 point2, Vector3 n, float penetration, bool isNewContact)
        {
            this.world = world;
            newContact = isNewContact;

            ShapeA = shapeA;
            ShapeB = shapeB;
            var body1 = ShapeA.Body;
            var body2 = ShapeB.Body;
            
            normal = n;
            Vector3.Multiply(ref normal, -0.1f, out normal);
            normal.Normalize();
            p1 = point1; p2 = point2;
                        
            Vector3.Subtract(ref p1, ref body1.currentState.transform.position, out relativePos1);
            Vector3.Subtract(ref p2, ref body2.currentState.transform.position, out relativePos2);
            Vector3.Transform(ref relativePos1, ref body1.invOrientation, out realRelPos1);
            Vector3.Transform(ref relativePos2, ref body2.invOrientation, out realRelPos2);
            
            initialPen = penetration;
            this.penetration = penetration;

            body1IsMassPoint = ShapeA.Body.IsParticle;
            body2IsMassPoint = ShapeB.Body.IsParticle;

            WithStatic = withStatic;
            
            treatBody1AsStatic = body1.IsStatic;
            treatBody2AsStatic = body2.IsStatic;

            accumulatedNormalImpulse = 0.0f;
            accumulatedTangentImpulse = 0.0f;

            lostSpeculativeBounce = 0.0f;

            switch (settings.MaterialCoefficientMixing)
            {
                case ContactSettings.MaterialCoefficientMixingType.TakeMaximum:
                    staticFriction = Mathf.Max(body1.material.staticFriction, body2.material.staticFriction);
                    dynamicFriction = Mathf.Max(body1.material.kineticFriction, body2.material.kineticFriction);
                    restitution = Mathf.Max(body1.material.restitution, body2.material.restitution);
                    break;
                case ContactSettings.MaterialCoefficientMixingType.TakeMinimum:
                    staticFriction = Mathf.Min(body1.material.staticFriction, body2.material.staticFriction);
                    dynamicFriction = Mathf.Min(body1.material.kineticFriction, body2.material.kineticFriction);
                    restitution = Mathf.Min(body1.material.restitution, body2.material.restitution);
                    break;
                case ContactSettings.MaterialCoefficientMixingType.UseAverage:
                    staticFriction = (body1.material.staticFriction + body2.material.staticFriction) / 2.0f;
                    dynamicFriction = (body1.material.kineticFriction + body2.material.kineticFriction) / 2.0f;
                    restitution = (body1.material.restitution + body2.material.restitution) / 2.0f;
                    break;
            }
            
            return this;
        }

        internal void PrepareForIteration()
        {
            if (liteTest)
                return;
            var body1 = ShapeA.Body;
            var body2 = ShapeB.Body;
            
            float dvx, dvy, dvz;

            dvx = (body2.angularVelocity.y * relativePos2.z) - (body2.angularVelocity.z * relativePos2.y) + body2.linearVelocity.x;
            dvy = (body2.angularVelocity.z * relativePos2.x) - (body2.angularVelocity.x * relativePos2.z) + body2.linearVelocity.y;
            dvz = (body2.angularVelocity.x * relativePos2.y) - (body2.angularVelocity.y * relativePos2.x) + body2.linearVelocity.z;

            dvx = dvx - (body1.angularVelocity.y * relativePos1.z) + (body1.angularVelocity.z * relativePos1.y) - body1.linearVelocity.x;
            dvy = dvy - (body1.angularVelocity.z * relativePos1.x) + (body1.angularVelocity.x * relativePos1.z) - body1.linearVelocity.y;
            dvz = dvz - (body1.angularVelocity.x * relativePos1.y) + (body1.angularVelocity.y * relativePos1.x) - body1.linearVelocity.z;

            float kNormal = 0.0f;

            Vector3 rantra = Vector3.zero;
            if (!treatBody1AsStatic)
            {
                kNormal += body1.inverseMass;

                if (!body1IsMassPoint)
                {

                    // Vector3.Cross(ref relativePos1, ref normal, out rantra);
                    rantra.x = (relativePos1.y * normal.z) - (relativePos1.z * normal.y);
                    rantra.y = (relativePos1.z * normal.x) - (relativePos1.x * normal.z);
                    rantra.z = (relativePos1.x * normal.y) - (relativePos1.y * normal.x);

                    // Vector3.Transform(ref rantra, ref body1.invInertiaWorld, out rantra);
                    float num0 = ((rantra.x * body1.invInertiaWorld.ex.x) + (rantra.y * body1.invInertiaWorld.ey.x)) + (rantra.z * body1.invInertiaWorld.ez.x);
                    float num1 = ((rantra.x * body1.invInertiaWorld.ex.y) + (rantra.y * body1.invInertiaWorld.ey.y)) + (rantra.z * body1.invInertiaWorld.ez.y);
                    float num2 = ((rantra.x * body1.invInertiaWorld.ex.z) + (rantra.y * body1.invInertiaWorld.ey.z)) + (rantra.z * body1.invInertiaWorld.ez.z);

                    rantra.x = num0; rantra.y = num1; rantra.z = num2;

                    //Vector3.Cross(ref rantra, ref relativePos1, out rantra);
                    num0 = (rantra.y * relativePos1.z) - (rantra.z * relativePos1.y);
                    num1 = (rantra.z * relativePos1.x) - (rantra.x * relativePos1.z);
                    num2 = (rantra.x * relativePos1.y) - (rantra.y * relativePos1.x);

                    rantra.x = num0; rantra.y = num1; rantra.z = num2;
                }
            }

            Vector3 rbntrb = Vector3.zero;
            if (!treatBody2AsStatic)
            {
                kNormal += body2.inverseMass;

                if (!body2IsMassPoint)
                {

                    // Vector3.Cross(ref relativePos1, ref normal, out rantra);
                    rbntrb.x = (relativePos2.y * normal.z) - (relativePos2.z * normal.y);
                    rbntrb.y = (relativePos2.z * normal.x) - (relativePos2.x * normal.z);
                    rbntrb.z = (relativePos2.x * normal.y) - (relativePos2.y * normal.x);

                    // Vector3.Transform(ref rantra, ref body1.invInertiaWorld, out rantra);
                    float num0 = ((rbntrb.x * body2.invInertiaWorld.ex.x) + (rbntrb.y * body2.invInertiaWorld.ey.x)) + (rbntrb.z * body2.invInertiaWorld.ez.x);
                    float num1 = ((rbntrb.x * body2.invInertiaWorld.ex.y) + (rbntrb.y * body2.invInertiaWorld.ey.y)) + (rbntrb.z * body2.invInertiaWorld.ez.y);
                    float num2 = ((rbntrb.x * body2.invInertiaWorld.ex.z) + (rbntrb.y * body2.invInertiaWorld.ey.z)) + (rbntrb.z * body2.invInertiaWorld.ez.z);

                    rbntrb.x = num0; rbntrb.y = num1; rbntrb.z = num2;

                    //Vector3.Cross(ref rantra, ref relativePos1, out rantra);
                    num0 = (rbntrb.y * relativePos2.z) - (rbntrb.z * relativePos2.y);
                    num1 = (rbntrb.z * relativePos2.x) - (rbntrb.x * relativePos2.z);
                    num2 = (rbntrb.x * relativePos2.y) - (rbntrb.y * relativePos2.x);

                    rbntrb.x = num0; rbntrb.y = num1; rbntrb.z = num2;
                }
            }

            if (!treatBody1AsStatic) kNormal += rantra.x * normal.x + rantra.y * normal.y + rantra.z * normal.z;
            if (!treatBody2AsStatic) kNormal += rbntrb.x * normal.x + rbntrb.y * normal.y + rbntrb.z * normal.z;

            massNormal = 1.0f / kNormal;

            float num = dvx * normal.x + dvy * normal.y + dvz * normal.z;

            tangent.x = dvx - normal.x * num;
            tangent.y = dvy - normal.y * num;
            tangent.z = dvz - normal.z * num;

            num = tangent.x * tangent.x + tangent.y * tangent.y + tangent.z * tangent.z;

            if (num != 0.0f)
            {
                num = (float)Mathf.Sqrt(num);
                tangent.x /= num;
                tangent.y /= num;
                tangent.z /= num;
            }

            float kTangent = 0.0f;

            if (treatBody1AsStatic) rantra.MakeZero();
            else
            {
                kTangent += body1.inverseMass;
  
                if (!body1IsMassPoint)
                {
                    // Vector3.Cross(ref relativePos1, ref normal, out rantra);
                    rantra.x = (relativePos1.y * tangent.z) - (relativePos1.z * tangent.y);
                    rantra.y = (relativePos1.z * tangent.x) - (relativePos1.x * tangent.z);
                    rantra.z = (relativePos1.x * tangent.y) - (relativePos1.y * tangent.x);

                    // Vector3.Transform(ref rantra, ref body1.invInertiaWorld, out rantra);
                    float num0 = ((rantra.x * body1.invInertiaWorld.ex.x) + (rantra.y * body1.invInertiaWorld.ey.x)) + (rantra.z * body1.invInertiaWorld.ez.x);
                    float num1 = ((rantra.x * body1.invInertiaWorld.ex.y) + (rantra.y * body1.invInertiaWorld.ey.y)) + (rantra.z * body1.invInertiaWorld.ez.y);
                    float num2 = ((rantra.x * body1.invInertiaWorld.ex.z) + (rantra.y * body1.invInertiaWorld.ey.z)) + (rantra.z * body1.invInertiaWorld.ez.z);

                    rantra.x = num0; rantra.y = num1; rantra.z = num2;

                    //Vector3.Cross(ref rantra, ref relativePos1, out rantra);
                    num0 = (rantra.y * relativePos1.z) - (rantra.z * relativePos1.y);
                    num1 = (rantra.z * relativePos1.x) - (rantra.x * relativePos1.z);
                    num2 = (rantra.x * relativePos1.y) - (rantra.y * relativePos1.x);

                    rantra.x = num0; rantra.y = num1; rantra.z = num2;
                }

            }

            if (treatBody2AsStatic) rbntrb.MakeZero();
            else
            {
                kTangent += body2.inverseMass;

                if (!body2IsMassPoint)
                {
                    // Vector3.Cross(ref relativePos1, ref normal, out rantra);
                    rbntrb.x = (relativePos2.y * tangent.z) - (relativePos2.z * tangent.y);
                    rbntrb.y = (relativePos2.z * tangent.x) - (relativePos2.x * tangent.z);
                    rbntrb.z = (relativePos2.x * tangent.y) - (relativePos2.y * tangent.x);

                    // Vector3.Transform(ref rantra, ref body1.invInertiaWorld, out rantra);
                    float num0 = ((rbntrb.x * body2.invInertiaWorld.ex.x) + (rbntrb.y * body2.invInertiaWorld.ey.x)) + (rbntrb.z * body2.invInertiaWorld.ez.x);
                    float num1 = ((rbntrb.x * body2.invInertiaWorld.ex.y) + (rbntrb.y * body2.invInertiaWorld.ey.y)) + (rbntrb.z * body2.invInertiaWorld.ez.y);
                    float num2 = ((rbntrb.x * body2.invInertiaWorld.ex.z) + (rbntrb.y * body2.invInertiaWorld.ey.z)) + (rbntrb.z * body2.invInertiaWorld.ez.z);

                    rbntrb.x = num0; rbntrb.y = num1; rbntrb.z = num2;

                    //Vector3.Cross(ref rantra, ref relativePos1, out rantra);
                    num0 = (rbntrb.y * relativePos2.z) - (rbntrb.z * relativePos2.y);
                    num1 = (rbntrb.z * relativePos2.x) - (rbntrb.x * relativePos2.z);
                    num2 = (rbntrb.x * relativePos2.y) - (rbntrb.y * relativePos2.x);

                    rbntrb.x = num0; rbntrb.y = num1; rbntrb.z = num2;
                }
            }

            if (!treatBody1AsStatic) kTangent += Vector3.Dot(ref rantra, ref tangent);
            if (!treatBody2AsStatic) kTangent += Vector3.Dot(ref rbntrb, ref tangent);
            massTangent = 1.0f / kTangent;

            restitutionBias = lostSpeculativeBounce;

            speculativeVelocity = 0.0f;

            float relNormalVel = normal.x * dvx + normal.y * dvy + normal.z * dvz; //Vector3.Dot(ref normal, ref dv);

            if (penetration > settings.allowedPenetration)
            {
                restitutionBias = settings.bias * (1.0f / SeclConfig.TARGET_TIMESTEP) * Mathf.Max(0.0f, penetration - settings.allowedPenetration);
                restitutionBias = Mathf.Clamp(restitutionBias, 0.0f, settings.maximumBias);
              //  body1IsMassPoint = body2IsMassPoint = false;
            }
      

//            float timeStepRatio = timestep / lastTimeStep;
//            accumulatedNormalImpulse *= timeStepRatio;
//            accumulatedTangentImpulse *= timeStepRatio;

            {
                // Static/Dynamic friction
                float relTangentVel = -(tangent.x * dvx + tangent.y * dvy + tangent.z * dvz);
                float tangentImpulse = massTangent * relTangentVel;
                float maxTangentImpulse = -staticFriction * accumulatedNormalImpulse;

                if (tangentImpulse < maxTangentImpulse) friction = dynamicFriction;
                else friction = staticFriction;
            }

            Vector3 impulse;

            // Simultaneos solving and restitution is simply not possible
            // so fake it a bit by just applying restitution impulse when there
            // is a new contact.
            if (relNormalVel < -1.0f && newContact)
            {
                restitutionBias = Mathf.Max(-restitution * relNormalVel, restitutionBias);
            }

            // Speculative Contacts!
            // if the penetration is negative (which means the bodies are not already in contact, but they will
            // be in the future) we store the current bounce bias in the variable 'lostSpeculativeBounce'
            // and apply it the next frame, when the speculative contact was already solved.
            if (penetration < -settings.allowedPenetration)
            {
                speculativeVelocity = penetration / SeclConfig.TARGET_TIMESTEP;

                lostSpeculativeBounce = restitutionBias;
                restitutionBias = 0.0f;
            }
            else
            {
                lostSpeculativeBounce = 0.0f;
            }

            impulse.x = normal.x * accumulatedNormalImpulse + tangent.x * accumulatedTangentImpulse;
            impulse.y = normal.y * accumulatedNormalImpulse + tangent.y * accumulatedTangentImpulse;
            impulse.z = normal.z * accumulatedNormalImpulse + tangent.z * accumulatedTangentImpulse;

            if (!treatBody1AsStatic)
            {
                body1.linearVelocity.x -= (impulse.x * body1.inverseMass);
                body1.linearVelocity.y -= (impulse.y * body1.inverseMass);
                body1.linearVelocity.z -= (impulse.z * body1.inverseMass);

                if (!body1IsMassPoint)
                {
                    float num0, num1, num2;
                    num0 = relativePos1.y * impulse.z - relativePos1.z * impulse.y;
                    num1 = relativePos1.z * impulse.x - relativePos1.x * impulse.z;
                    num2 = relativePos1.x * impulse.y - relativePos1.y * impulse.x;

                    float num3 =
                        (((num0 * body1.invInertiaWorld.ex.x) +
                        (num1 * body1.invInertiaWorld.ey.x)) +
                        (num2 * body1.invInertiaWorld.ez.x));
                    float num4 =
                        (((num0 * body1.invInertiaWorld.ex.y) +
                        (num1 * body1.invInertiaWorld.ey.y)) +
                        (num2 * body1.invInertiaWorld.ez.y));
                    float num5 =
                        (((num0 * body1.invInertiaWorld.ex.z) +
                        (num1 * body1.invInertiaWorld.ey.z)) +
                        (num2 * body1.invInertiaWorld.ez.z));

                    body1.angularVelocity.x -= num3;
                    body1.angularVelocity.y -= num4;
                    body1.angularVelocity.z -= num5;

                }
            }

            if (!treatBody2AsStatic)
            {

                body2.linearVelocity.x += (impulse.x * body2.inverseMass);
                body2.linearVelocity.y += (impulse.y * body2.inverseMass);
                body2.linearVelocity.z += (impulse.z * body2.inverseMass);

                if (!body2IsMassPoint)
                {

                    float num0, num1, num2;
                    num0 = relativePos2.y * impulse.z - relativePos2.z * impulse.y;
                    num1 = relativePos2.z * impulse.x - relativePos2.x * impulse.z;
                    num2 = relativePos2.x * impulse.y - relativePos2.y * impulse.x;

                    float num3 =
                        (((num0 * body2.invInertiaWorld.ex.x) +
                        (num1 * body2.invInertiaWorld.ey.x)) +
                        (num2 * body2.invInertiaWorld.ez.x));
                    float num4 =
                        (((num0 * body2.invInertiaWorld.ex.y) +
                        (num1 * body2.invInertiaWorld.ey.y)) +
                        (num2 * body2.invInertiaWorld.ez.y));
                    float num5 =
                        (((num0 * body2.invInertiaWorld.ex.z) +
                        (num1 * body2.invInertiaWorld.ey.z)) +
                        (num2 * body2.invInertiaWorld.ez.z));

                    body2.angularVelocity.x += num3;
                    body2.angularVelocity.y += num4;
                    body2.angularVelocity.z += num5;
                }
            }
        }

        internal void IteratePlus()
        {
//            Assign(world, ShapeA, ShapeB,)
        }
        
        internal void Iterate()
        {
            if (liteTest)
                return;
            if (treatBody1AsStatic && treatBody2AsStatic) return;

            var body1 = ShapeA.Body;
            var body2 = ShapeB.Body;

            float dvx, dvy, dvz;

            dvx = body2.linearVelocity.x - body1.linearVelocity.x;
            dvy = body2.linearVelocity.y - body1.linearVelocity.y;
            dvz = body2.linearVelocity.z - body1.linearVelocity.z;

            if (!body1IsMassPoint)
            {
                dvx = dvx - (body1.angularVelocity.y * relativePos1.z) + (body1.angularVelocity.z * relativePos1.y);
                dvy = dvy - (body1.angularVelocity.z * relativePos1.x) + (body1.angularVelocity.x * relativePos1.z);
                dvz = dvz - (body1.angularVelocity.x * relativePos1.y) + (body1.angularVelocity.y * relativePos1.x);
            }

            if (!body2IsMassPoint)
            {
                dvx = dvx + (body2.angularVelocity.y * relativePos2.z) - (body2.angularVelocity.z * relativePos2.y);
                dvy = dvy + (body2.angularVelocity.z * relativePos2.x) - (body2.angularVelocity.x * relativePos2.z);
                dvz = dvz + (body2.angularVelocity.x * relativePos2.y) - (body2.angularVelocity.y * relativePos2.x);
            }

            // this gets us some performance
            if (dvx * dvx + dvy * dvy + dvz * dvz < settings.minVelocity * settings.minVelocity)
            { return; }

            float vn = normal.x * dvx + normal.y * dvy + normal.z * dvz;
            float normalImpulse = massNormal * (-vn + restitutionBias + speculativeVelocity);

            float oldNormalImpulse = accumulatedNormalImpulse;
            accumulatedNormalImpulse = oldNormalImpulse + normalImpulse;
            if (accumulatedNormalImpulse < 0.0f) accumulatedNormalImpulse = 0.0f;
            normalImpulse = accumulatedNormalImpulse - oldNormalImpulse;

            float vt = dvx * tangent.x + dvy * tangent.y + dvz * tangent.z;
            float maxTangentImpulse = friction * accumulatedNormalImpulse;
            float tangentImpulse = massTangent * (-vt);

            float oldTangentImpulse = accumulatedTangentImpulse;
            accumulatedTangentImpulse = oldTangentImpulse + tangentImpulse;
            if (accumulatedTangentImpulse < -maxTangentImpulse) accumulatedTangentImpulse = -maxTangentImpulse;
            else if (accumulatedTangentImpulse > maxTangentImpulse) accumulatedTangentImpulse = maxTangentImpulse;

            tangentImpulse = accumulatedTangentImpulse - oldTangentImpulse;

            // Apply contact impulse
            Vector3 impulse;
            impulse.x = normal.x * normalImpulse + tangent.x * tangentImpulse;
            impulse.y = normal.y * normalImpulse + tangent.y * tangentImpulse;
            impulse.z = normal.z * normalImpulse + tangent.z * tangentImpulse;

            if (!treatBody1AsStatic)
            {
                body1.linearVelocity.x -= (impulse.x * body1.inverseMass);
                body1.linearVelocity.y -= (impulse.y * body1.inverseMass);
                body1.linearVelocity.z -= (impulse.z * body1.inverseMass);
                

                if (!body1IsMassPoint)
                {
                    float num0, num1, num2;
                    num0 = relativePos1.y * impulse.z - relativePos1.z * impulse.y;
                    num1 = relativePos1.z * impulse.x - relativePos1.x * impulse.z;
                    num2 = relativePos1.x * impulse.y - relativePos1.y * impulse.x;

                    float num3 =
                        (((num0 * body1.invInertiaWorld.ex.x) +
                        (num1 * body1.invInertiaWorld.ey.x)) +
                        (num2 * body1.invInertiaWorld.ez.x));
                    float num4 =
                        (((num0 * body1.invInertiaWorld.ex.y) +
                        (num1 * body1.invInertiaWorld.ey.y)) +
                        (num2 * body1.invInertiaWorld.ez.y));
                    float num5 =
                        (((num0 * body1.invInertiaWorld.ex.z) +
                        (num1 * body1.invInertiaWorld.ey.z)) +
                        (num2 * body1.invInertiaWorld.ez.z));

                    body1.angularVelocity.x -= num3;
                    body1.angularVelocity.y -= num4;
                    body1.angularVelocity.z -= num5;
                }
            }

            if (!treatBody2AsStatic)
            {

                body2.linearVelocity.x += (impulse.x * body2.inverseMass);
                body2.linearVelocity.y += (impulse.y * body2.inverseMass);
                body2.linearVelocity.z += (impulse.z * body2.inverseMass);
                

                if (!body2IsMassPoint)
                {

                    float num0, num1, num2;
                    num0 = relativePos2.y * impulse.z - relativePos2.z * impulse.y;
                    num1 = relativePos2.z * impulse.x - relativePos2.x * impulse.z;
                    num2 = relativePos2.x * impulse.y - relativePos2.y * impulse.x;

                    float num3 =
                        (((num0 * body2.invInertiaWorld.ex.x) +
                        (num1 * body2.invInertiaWorld.ey.x)) +
                        (num2 * body2.invInertiaWorld.ez.x));
                    float num4 =
                        (((num0 * body2.invInertiaWorld.ex.y) +
                        (num1 * body2.invInertiaWorld.ey.y)) +
                        (num2 * body2.invInertiaWorld.ez.y));
                    float num5 =
                        (((num0 * body2.invInertiaWorld.ex.z) +
                        (num1 * body2.invInertiaWorld.ey.z)) +
                        (num2 * body2.invInertiaWorld.ez.z));

                    body2.angularVelocity.x += num3;
                    body2.angularVelocity.y += num4;
                    body2.angularVelocity.z += num5;

                }
            }
//            body1.Integrate(false);
//            body2.Integrate(false);
//            UpdatePosition();
        }
        
        public void UpdatePosition()
        {
            var body1 = ShapeA.Body;
            var body2 = ShapeB.Body;
            
            if (body1IsMassPoint)
            {
                Vector3.Add(ref realRelPos1, ref body1.currentState.transform.position, out p1);
            }
            else
            {
                Vector3.Transform(ref realRelPos1, ref body1.currentState.transform.orientation, out p1);
                Vector3.Add(ref p1, ref body1.currentState.transform.position, out p1);
            }

            if (body2IsMassPoint)
            {
                Vector3.Add(ref realRelPos2, ref body2.currentState.transform.position, out p2);
            }
            else
            {
                Vector3.Transform(ref realRelPos2, ref body2.currentState.transform.orientation, out p2);
                Vector3.Add(ref p2, ref body2.currentState.transform.position, out p2);
            }


            Vector3 dist; Vector3.Subtract(ref p1, ref p2, out dist);
            penetration = Vector3.Dot(ref dist, ref normal);
        }

        private void Reset()
        {
            liteTest = false;
            ShapeA = null;
            ShapeB = null;
            WithStatic = false;
            penetration = 0.0f;
            initialPen = 0.0f;

            normal = default;
            tangent = default;

            realRelPos1 = default;
            realRelPos2 = default;
            relativePos1 = default;
            relativePos2 = default;
            p1 = default;
            p2 = default;

            accumulatedNormalImpulse = default;
            accumulatedTangentImpulse = default;
            staticFriction = default;
            dynamicFriction = default;
            restitution = default;
            friction = default;
            massNormal = default;
            massTangent = default;
            restitutionBias = default;

            treatBody1AsStatic = default;
            treatBody2AsStatic = default;
            body1IsMassPoint = default;
            body2IsMassPoint = default;

            lostSpeculativeBounce = default;
            speculativeVelocity = default;

            world = null;
        }
    }
}