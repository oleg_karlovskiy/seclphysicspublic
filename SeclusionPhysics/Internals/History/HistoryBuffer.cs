﻿using System;
using System.Collections.Generic;
using SeclusionPhysics.Utils;

namespace SeclusionPhysics.Internals.History
{
  internal class HistoryBuffer : IVoltPoolable<HistoryBuffer>
  {
    #region Interface
    ISeclPool<HistoryBuffer> IVoltPoolable<HistoryBuffer>.Pool { get; set; }
    void IVoltPoolable<HistoryBuffer>.Reset() { Reset(); }
    #endregion

    private HistoryRecord[] data;
    private int capacity;
    int count;
    int start;

    public int Capacity { get { return capacity; } }
//
    public HistoryBuffer()
    {
      data = null;
      capacity = 0;
      count = 0;
      start = 0;
    }

    public void Initialize(int capacity)
    {
      if ((data == null) || (data.Length < capacity))
        data = new HistoryRecord[capacity];
      this.capacity = capacity;
      count = 0;
      start = 0;
    }

    private void Reset()
    {
      count = 0;
      start = 0;
    }

    /// <summary>
    /// Stores a value as latest.
    /// </summary>
    public void Store(HistoryRecord value)
    {
      if (count < capacity)
      {
        data[count++] = value;
        IncrementStart();
      }
      else
      {
        data[start] = value;
        IncrementStart();
      }
    }
    
    public void ReplaceCurrent(HistoryRecord value)
    {
      if (count < capacity)
      {       
        data[count == 0 ? 0 : count - 1] = value;
      }
      else
      {
        data[start == 0 ? 0 : start - 1] = value;
      }
    }

    /// <summary>
    /// Tries to get a value with a given number of frames behind the last 
    /// value stored. If the value can't be foundfunction will find
    /// the closest and return false, indicating a clamp.
    /// </summary>
    public bool TryGet(int numBehind, out HistoryRecord value)
    {
      if (numBehind < 0)
        throw new ArgumentOutOfRangeException("numBehind");

      if (count < capacity)
      {
        if (numBehind >= count)
        {
          value = data[0];
          return false;
        }

        value = data[count - numBehind - 1];
        return true;
      }
      else
      {
        bool found = true;
        if (numBehind >= capacity)
        {
          numBehind = capacity - 1;
          found = false;
        }

        int index =
          ((start - numBehind - 1) + capacity)
          % capacity;
        value = data[index];
        return found;
      }
    }

    /// <summary>
    /// Returns all values, but not in order.
    /// </summary>
    public IEnumerable<HistoryRecord> GetValues()
    {
      for (int i = 0; i < count; i++)
        yield return data[i];
    }

    private void IncrementStart()
    {
      start = (start + 1) % capacity;
    }
  }
}
