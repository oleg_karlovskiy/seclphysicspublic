﻿using SeclusionPhysics.Structs;
using VionisEssencesSystem.GameCore.MiscStructs;

namespace SeclusionPhysics.Internals.History
{
    // <summary>
    // A stored historical image of a past body state, used for historical
    // queries and raycasts. Rather than actually rolling the body back to
    // its old position (expensive), we transform the ray into the body's
    // local space based on the body's old position/axis. Then all casts
    // on shapes use the local-space ray (this applies both for current-
    // time and past-time raycasts and point queries).
    // </summary>
    public struct HistoryRecord
    {
        internal VoltAABB aabb;
        internal Transform transform;

        internal void Store(ref HistoryRecord other)
        {
            aabb = other.aabb;
            transform = other.transform;
        }

        public void Stabilize()
        {
            transform.Stabilize();
        }
    }
}