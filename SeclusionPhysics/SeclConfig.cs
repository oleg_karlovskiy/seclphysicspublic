namespace SeclusionPhysics
{
    public class SeclConfig
    {
        internal const float DEFAULT_LINEAR_DAMPING = 0.99f;
        internal const float DEFAULT_ANGULAR_DAMPING = 0.99f;
        internal const int ITERATIONS_FOR_STATIC_IMPACT_SLEEP = 15;
        
        internal const float POSOFFSET_FOR_STATIC_IMPACT_SLEEP = 0.01f;
        internal const float MATOFFSET_FOR_STATIC_IMPACT_SLEEP = 0.01f;
        
        internal const int DETERMINISM_PRECISION = 2;
        
        public const int TARGET_FPS = 30;
        internal const float TARGET_TIMESTEP = 1f / 30;

        // AABB extension for the dynamic tree
        internal const float AABB_EXTENSION = 0.2f;

    }
}