﻿//using System;
//using System.Collections.Generic;
//
//using DonutMagic.Common.UnityImitation;
//using SeclusionPhysics.Structs;
//
//namespace Volatile
//{
//  public struct VoltRayResult
//  {
//    public bool IsValid { get { return this.shape != null; } }
//    public bool IsContained
//    {
//      get { return this.IsValid && this.distance == 0.0f; }
//    }
//
//    public VoltShape Shape { get { return this.shape; } }
//
//    public VoltBody Body 
//    { 
//      get { return (this.shape == null) ? null : this.shape.Body; } 
//    }
//
//    public float Distance { get { return this.distance; } }
//    public Vector2 Normal { get { return this.normal; } }
//
//    private VoltShape shape;
//    private float distance;
//    internal Vector2 normal;
//
//    public Vector2 ComputePoint(ref VoltRayCast cast)
//    {
//      return cast.origin + (cast.direction * this.distance);
//    }
//
//    internal void Set(
//      VoltShape shape,
//      float distance,
//      Vector2 normal)
//    {
//      if (this.IsValid == false || distance < this.distance)
//      {
//        this.shape = shape;
//        this.distance = distance;
//        this.normal = normal;
//      }
//    }
//
//    internal void Reset()
//    {
//      this.shape = null;
//    }
//
//    internal void SetContained(VoltShape shape)
//    {
//      this.shape = shape;
//      this.distance = 0.0f;
//      this.normal = Vector2.zero;
//    }
//  }
//}