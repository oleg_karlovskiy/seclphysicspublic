﻿//using System;
//using System.Collections.Generic;
//
//using DonutMagic.Common.UnityImitation;
//
//namespace Volatile
//{
//  /// <summary>
//  /// A semi-precomputed ray optimized for fast AABB tests.
//  /// </summary>
//  public struct VoltRayCast
//  {
//    internal readonly Vector2 origin;
//    internal readonly Vector2 direction;
//    internal readonly Vector2 invDirection;
//    internal readonly float distance;
//    internal readonly bool signX;
//    internal readonly bool signY;
//
//    public VoltRayCast(Vector2 origin, Vector2 destination)
//    {
//      Vector2 delta = destination - origin;
//
//      this.origin = origin;
//      this.direction = delta.normalized;
//      this.distance = delta.magnitude;
//      this.signX = direction.x < 0.0f;
//      this.signY = direction.y < 0.0f;
//      this.invDirection = 
//        new Vector2(1.0f / direction.x, 1.0f / direction.y);
//    }
//
//    public VoltRayCast(Vector2 origin, Vector2 direction, float distance)
//    {
//      this.origin = origin;
//      this.direction = direction;
//      this.distance = distance;
//      this.signX = direction.x < 0.0f;
//      this.signY = direction.y < 0.0f;
//      this.invDirection = 
//        new Vector2(1.0f / direction.x, 1.0f / direction.y);
//    }
//  }
//}