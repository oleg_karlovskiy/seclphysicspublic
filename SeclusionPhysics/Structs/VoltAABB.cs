using SeclusionPhysics.Utils;
using VionisEssencesSystem.GameCore.MiscStructs;
using Mathf = SeclusionPhysics.Utils.Mathf;

namespace SeclusionPhysics.Structs
{
    public struct VoltAABB
    {
        #region Static Methods

        public static readonly VoltAABB LargeBox;

        /// <summary>
        /// Returns the smalltest box possible.
        /// </summary>
        public static readonly VoltAABB SmallBox;

        static VoltAABB()
        {
            LargeBox.Min = new Vector3(float.MinValue, float.MinValue,float.MinValue);
            LargeBox.Max = new Vector3(float.MaxValue, float.MaxValue,float.MaxValue);
            SmallBox.Min = new Vector3(float.MaxValue, float.MaxValue,float.MaxValue);
            SmallBox.Max = new Vector3(float.MinValue, float.MinValue,float.MinValue);
        }
        
        public static VoltAABB CreateExpanded(VoltAABB aabb, float expansionAmount)
        {
            return new VoltAABB(aabb.Min.x - expansionAmount,
                            aabb.Min.y - expansionAmount,
                            aabb.Min.z - expansionAmount,
                            aabb.Max.x + expansionAmount,
                            aabb.Max.y + expansionAmount,
                            aabb.Max.z + expansionAmount);
        }

        public static VoltAABB CreateMerged(VoltAABB aabb1, VoltAABB aabb2)
        {
            return new VoltAABB(
                Mathf.Min(aabb1.Min.x, aabb2.Min.x),
                Mathf.Min(aabb1.Min.y, aabb2.Min.y),
                Mathf.Min(aabb1.Min.z, aabb2.Min.z),
                
                Mathf.Max(aabb1.Max.x, aabb2.Max.x),
                Mathf.Max(aabb1.Max.y, aabb2.Max.y),
                Mathf.Max(aabb1.Max.z, aabb2.Max.z));
        }

        #endregion

        public Vector3 Min;
        public Vector3 Max;
        
        public float Perimeter;

        public VoltAABB(float minX, float minY, float minZ, float maxX, float maxY, float maxZ)
        {
            Min = new Vector3(minX, minY, minZ);
            Max = new Vector3(maxX, maxY, maxZ);
            Perimeter = CalcPerimeter(Min, Max);
        }
        
        
        public VoltAABB(Vector3 center, Vector3 extents)
        {
            var mins = center - extents;
            var maxs = center + extents;

            Min = new Vector3(mins.x, mins.y, mins.z);
            Max = new Vector3(maxs.x, maxs.y, maxs.z);
            Perimeter = CalcPerimeter(Min, Max);
        }

        private static float CalcPerimeter(Vector3 min, Vector3 max)
        {
            return 2.0f * ((max.x - min.x) * (max.y - min.y) +
                                (max.x - min.x) * (max.z - min.z) +
                                (max.z - min.z) * (max.y - min.y));
        }

        #region Tests

        public bool QueryPoint(Vector3 point)
        {
            return point.x >= Min.x && point.x <= Max.x && 
                   point.y >= Min.y && point.y <= Max.y && 
                   point.z >= Min.z && point.z <= Max.z;
        }
        
        public bool Intersect(VoltAABB other)
        {
            return other.Min.x <= Max.x && other.Max.x >= Min.x &&
                   other.Min.y <= Max.y && other.Max.y >= Min.y &&
                   other.Min.z <= Max.z && other.Max.z >= Min.z;
        }

        public bool Contains(VoltAABB other)
        {
            return Max.x >= other.Max.x &&
                   Max.y >= other.Max.y &&
                   Max.z >= other.Max.z &&
                   Min.x <= other.Min.x &&
                   Min.y <= other.Min.y &&
                   Min.z <= other.Min.z;
        }

        #endregion
        
        public void Transform(ref Mat3 orientation)
        {
            Vector3 halfExtents = 0.5f * (Max - Min);
            Vector3 center = 0.5f * (Max + Min);

            Vector3.Transform(ref center, ref orientation, out center);

            Mat3 abs; Mat3.Absolute(ref orientation, out abs);
            Vector3.Transform(ref halfExtents, ref abs, out halfExtents);

            Max = center + halfExtents;
            Min = center - halfExtents;
        }
        
        internal void InverseTransform(ref Vector3 position, ref Mat3 orientation)
        {
            Vector3.Subtract(ref Max, ref position, out Max);
            Vector3.Subtract(ref Min, ref position, out Min);

            Vector3 center;
            Vector3.Add(ref Max, ref Min, out center);
            center.x *= 0.5f; center.y *= 0.5f; center.z *= 0.5f;

            Vector3 halfExtents;
            Vector3.Subtract(ref Max, ref Min, out halfExtents);
            halfExtents.x *= 0.5f; halfExtents.y *= 0.5f; halfExtents.z *= 0.5f;

            Vector3.TransposedTransform(ref center, ref orientation, out center);

            Mat3 abs; Mat3.Absolute(ref orientation, out abs);
            Vector3.TransposedTransform(ref halfExtents, ref abs, out halfExtents);

            Vector3.Add(ref center, ref halfExtents, out Max);
            Vector3.Subtract(ref center, ref halfExtents, out Min);
        }
        
        public void Transform(ref Vector3 vec)
        {
            Max += vec;
            Min += vec;
        }
        
        public void DebugDraw(IDebugDrawer d, Color c)
        {
            d.SetColor(c);
            var delta = (Max - Min) / 2;
            var pos = Min + delta;
            d.DrawBox(new Transform
            {
                position = pos,
                orientation = Mat3.Identity
            }, delta);
        }

        public void AddPoint(ref Vector3 point)
        {
            Vector3.Max(ref Max, ref point, out Max);
            Vector3.Min(ref Min, ref point, out Min);
        }
    }
}